#coding:utf-8
from django.db import models

# 权限表
class Permissions(models.Model):
    permissionid = models.AutoField(primary_key=True)
    permission = models.CharField(max_length=100, blank=True)
    class Meta:
        db_table = 'permissions'
        ordering = ['-permissionid']
    def __unicode__(self):
        return 'permissionid:%s  permission:%s' %(self.permissionid,self.permission)

# 市场表
class Markets(models.Model):
    marketid = models.CharField(max_length=100, primary_key=True)
    marketname = models.CharField(max_length=100, blank=True)
    tel = models.CharField(max_length=100, blank=True)
    class Meta:
        db_table = 'markets'
        ordering = ['-marketid']
    def __unicode__(self):
        return 'marketid:%s  marketname:%s' %(self.marketid,self.marketname)
    
# 用户表
class Users(models.Model):
    userid = models.AutoField(primary_key=True)
    username = models.CharField(max_length=100, blank=True)
    password = models.CharField(max_length=100, blank=True)
    name = models.CharField(max_length=100, blank=True)
    marketid = models.ForeignKey(Markets, db_column='marketid')
    permissionid = models.ForeignKey(Permissions, db_column='permissionid')
    class Meta:
        db_table = 'users'
        ordering = ['-userid']
    def __unicode__(self):
        return 'userid:%s  username:%s  password:%s  name:%s  marketid:%s  permissionid:%s ' %(self.userid,self.username,self.password,self.name,self.marketid,self.permissionid)

# 问题类型表
class QuestionType(models.Model):
    questiontypeid = models.AutoField(primary_key=True)
    questiontype = models.CharField(max_length=100, blank=True)
    class Meta:
        db_table = 'questiontype'
        ordering = ['-questiontypeid']
    def __unicode__(self):
        return 'questiontypeid:%s  questiontype:%s' %(self.questiontypeid,self.questiontype)

# 题库表
class Questions(models.Model):
    questionid = models.AutoField(primary_key=True)
    question = models.CharField(max_length=1000,blank=True, null=True)
    questiontypeid = models.ForeignKey(QuestionType, db_column='questiontypeid')
    choicetype = models.IntegerField('选择类型',blank=True, null=True)
    answers = models.CharField(max_length=1000)
    #marketid = models.ForeignKey(Markets, db_column='marketid')
    createtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'questions'
        ordering = ['-questionid']
    def __unicode__(self):
        return 'questionid:%s  question:%s  questiontypeid:%s  choicetype:%s  answers:%s  marketid:%s  createtime:%s '  %(self.questionid,self.question,self.question,self.choicetype,self.answers,self.marketid,self.createtime)

# 问卷表
class Questionnaires(models.Model):
    questionnaireid = models.AutoField(primary_key=True)
    questionids = models.CharField(max_length=1000, blank=True)
    marketid = models.ForeignKey(Markets, db_column='marketid')
    createtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'questionnaires'
        ordering = ['-questionnaireid']
    def __unicode__(self):
        return 'questionnaireid:%s  questionids:%s  marketid:%s  createtime:%s ' %(self.questionnaireid,self.questionids,self.marketid.marketid,self.createtime)

# 调查活动表
class Surveys(models.Model):
    surveyid = models.AutoField(primary_key=True) 
    marketid = models.ForeignKey(Markets, db_column='marketid')
    questionnaireid = models.ForeignKey(Questionnaires, db_column='questionnaireid')
    result = models.CharField(max_length=1000, blank=True)
    begintime = models.IntegerField(blank=True, null=True)
    endtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'surveys'
        ordering = ['-surveyid']
    def __unicode__(self):
        return 'surveyid:%s  marketid:%s  questionnaireid:%s  result:%s   begintime:%s  endtime:%s ' %(self.surveyid,self.marketid,self.questionnaireid,self.result,self.begintime,self.endtime)

# 活动表    
class Activities(models.Model):
    activityid = models.AutoField(primary_key=True) 
    marketid = models.ForeignKey(Markets, db_column='marketid')
    content = models.CharField(max_length=1000, blank=True)
    imagenames = models.CharField(max_length=1000, blank=True)
    begintime = models.DateField(blank=True, null=True)
    endtime = models.DateField(blank=True, null=True)
    class Meta:
        db_table = 'activities'
        ordering = ['-activityid']
    def __unicode__(self):
        return 'activityid:%s  marketid:%s  content:%s  imagenames:%s   begintime:%s  endtime:%s ' %(self.activityid,self.marketid,self.content,self.imagenames,self.begintime,self.endtime)
 
 # 配件价格表   
class Units(models.Model):
    unitid = models.AutoField(primary_key=True) 
    unitname = models.CharField(max_length=500, blank=True) 
    #price = models.IntegerField(blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    marketid = models.CharField(max_length=100, blank=True) 
    updatetime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'units'
        ordering = ['-unitid']
    def __unicode__(self):
        return 'unitid:%s  price:%s  update:%s' %(self.unitid,self.price,self.updatetime)

# 组合设计一字柜表
class Default_design_a(models.Model):
    designid = models.AutoField(primary_key=True) 
    base_designid = models.IntegerField(blank=True, null=True)
    designname1 = models.CharField(max_length=100, blank=True)
    designname2 = models.CharField(max_length=100, blank=True)
    designdesc = models.CharField(max_length=1000, blank=True)
    need_damping = models.IntegerField(blank=True, null=True)
    door_type = models.IntegerField(blank=True, null=True)
    cover = models.CharField(max_length=100, blank=True)
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    marketid = models.CharField(max_length=100, blank=True)
    show = models.IntegerField(blank=True, null=True)
    clickcount = models.IntegerField(blank=True, null=True)
    buycount = models.IntegerField(blank=True, null=True)
    updatetime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'default_design_a'
        ordering = ['-designid']
    def __unicode__(self):
        return 'designid:%s  old_designid:%s  designname:%s  marketid:%s  show:%s' %(self.designid,self.old_designid,self.designname, self.marketid, self.show)

# 组合设计转角柜表
class Default_design_b(models.Model):
    designid = models.AutoField(primary_key=True) 
    base_designid = models.IntegerField(blank=True, null=True)
    designname1 = models.CharField(max_length=100, blank=True)
    designname2 = models.CharField(max_length=100, blank=True)
    designdesc = models.CharField(max_length=1000, blank=True)
    need_damping = models.IntegerField(blank=True, null=True)
    door_type = models.IntegerField(blank=True, null=True)
    cover = models.CharField(max_length=100, blank=True)
    lx = models.IntegerField(blank=True, null=True)
    rx = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    lz = models.IntegerField(blank=True, null=True)
    rz = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    marketid = models.CharField(max_length=100, blank=True)
    show = models.IntegerField(blank=True, null=True)
    clickcount = models.IntegerField(blank=True, null=True)
    buycount = models.IntegerField(blank=True, null=True)
    updatetime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'default_design_b'
        ordering = ['-designid']
    def __unicode__(self):
        return 'designid:%s  old_designid:%s  designname:%s  marketid:%s  show:%s' %(self.designid,self.old_designid,self.designname, self.marketid, self.show)

# 推荐组合点击和购买统计表
class Default_design_statistics(models.Model):
    id = models.AutoField(primary_key=True) 
    designid = models.IntegerField(blank=True, null=True)
    designtype = models.CharField(max_length=10, blank=True)
    action = models.CharField(max_length=10, blank=True)
    createtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'default_design_statistics'
        ordering = ['-id']
    def __unicode__(self):
        return 'id:%s  designid:%s  designtype:%s  action:%s  createtime:%s' %(self.id,self.designid,self.designtype, self.action, self.createtime)

# 组合设计一字柜表
class Reference_design_a(models.Model):
    designid = models.AutoField(primary_key=True) 
    base_designid = models.IntegerField(blank=True, null=True) 
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    marketid = models.CharField(max_length=100, blank=True)
    createtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'reference_design_a'
        ordering = ['-designid']
    def __unicode__(self):
        return 'designid:%s  marketid:%s' %(self.designid, self.marketid)

# 组合设计转角柜表
class Reference_design_b(models.Model):
    designid = models.AutoField(primary_key=True) 
    base_designid = models.IntegerField(blank=True, null=True) 
    lx = models.IntegerField(blank=True, null=True)
    rx = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    lz = models.IntegerField(blank=True, null=True)
    rz = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    marketid = models.CharField(max_length=100, blank=True)
    createtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'reference_design_b'
        ordering = ['-designid']
    def __unicode__(self):
        return 'designid:%s  marketid:%s' %(self.designid, self.marketid)

# 自主设计一字柜表    
class Independent_design_a(models.Model):
    designid = models.AutoField(primary_key=True) 
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    marketid = models.CharField(max_length=100, blank=True)
    createtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'independent_design_a'
        ordering = ['-designid']
    def __unicode__(self):
        return 'designid:%s  marketid:%s' %(self.designid, self.marketid)

# 自主设计转角柜表 
class Independent_design_b(models.Model):
    designid = models.AutoField(primary_key=True) 
    lx = models.IntegerField(blank=True, null=True)
    rx = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    lz = models.IntegerField(blank=True, null=True)
    rz = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    marketid = models.CharField(max_length=100, blank=True)
    createtime = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'independent_design_b'
        ordering = ['-designid']
    def __unicode__(self):
        return 'designid:%s  marketid:%s' %(self.designid, self.marketid)
