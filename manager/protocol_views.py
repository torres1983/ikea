#coding:utf-8

import datetime
import json
import logging
import time
from datetime import date

import MySQLdb
from django.db import connection
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

from ikea.settings import *
from models import *
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
import re

import errno

import sys
reload(sys)
sys.setdefaultencoding('utf8')  


# 上传文件，三个参数分别是文件句柄、上传目录、文件名，其中文件名默认为上传时的文件名
def handle_uploaded_file(file, upload_dir, newfilename):
	logging.warning("handle_uploaded_file:  upload_dir:%s   newfilename:%s"  %(upload_dir,newfilename))
	destination = open(upload_dir + newfilename, 'wb')
	for chunk in file.chunks():
		destination.write(chunk)
	destination.close()

# 带图片附件的邮件发送函数封装
def send_mail_fun(to_user_email, imagefilename, tel):
	logging.warning("to_user_email:%s   imagefilename:%s"  %(to_user_email,imagefilename))
	#"""
	email_smtp_server ="smtp.163.com"  	# 发送者邮箱的 smtp 服务器	
	from_user_email = u"ikea_pax_planner@163.com"   # 发送者邮箱的帐号
	from_user_password ="ikea2014"   #发送者邮箱的密码
	"""
	email_smtp_server ="smtp.qq.com"  	# 发送者邮箱的 smtp 服务器
	from_user_email ="532570520@qq.com"   # 发送者邮箱的帐号
	from_user_password ="xingfu205811"   #发送者邮箱的密码
	"""
	from_user_name = "宜家PAX衣柜设计方案<ikea_pax_planner@163.com>"		#发件人显示的名字，也可以当作邮件标题（Title）
	#from_user_name = u"宜家PAX衣柜设计方案" 		#发件人显示的名字，也可以当作邮件标题（Title）
	email_subject = u"立即查看您在宜家家居设计的PAX衣柜清单"		# 邮件主题
	# 组织邮件信息
	msg = MIMEMultipart('related')
	msg['subject'] = email_subject
	msg['from'] = from_user_name
	msg['to'] = to_user_email	
	# 添加文字内容到邮件
	content = u"""
	<pre>
亲爱的用户，
感谢您使用我们的PAX衣柜设计APP，我们期待您再次光临宜家，为您提供更多优质的服务！
现在就点击附件下载并查看您设计的PAX衣柜清单吧！

*
如果想在家自行设计您的衣柜，请点击链接到官网进行设计 www.ikea.cn/paxplanner；
更多衣柜信息，请点击链接www.ikea.com/PAX  和 衣柜购物指南；
如需沟通送货及安装服务，可拨打客服热线 %s（人工服务时间9：00－21：00）。
<img src='cid:image1'>
	</pre>
	""" %tel
	content = MIMEText(content,_subtype='html',_charset='gb2312')
	msg.attach(content)	
	# 添加图片到邮件中
	image = MIMEImage(open('/htdocs/ikea/media/email/'+imagefilename,'rb').read())
	image.add_header('Content-ID', '<image1>')
	msg.attach(image)	
	"""
	# 添加附件到邮件
	attach_file = MIMEText(open('/htdocs/ikea/media/email/'+imagefilename).read(),'base64','gb2312')
	#attach_file["Content-Type"] = 'image/png'
	attach_file["Content-Type"] = 'application/octet-stream'
	attach_file["Content-Disposition"] = 'attachment; filename=u"设计清单.png"'
	msg.attach(attach_file)	
	"""	
	try:
		server = smtplib.SMTP()
		server.connect(email_smtp_server)
		logging.warning("will login email")
		server.login(from_user_email,from_user_password)
		logging.warning("login email ok")
		logging.warning("from_user_name:%s  to_user_email:%s" %(from_user_name,to_user_email))
		server.sendmail(from_user_email, to_user_email,msg.as_string())
		logging.warning("sendmail complete")
		server.close()
		return True
	except Exception, e:
		logging.warning("Exception:%s" %e)
		return False


# 获取服务器时间协议
@csrf_exempt  
def get_time(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			retdict["time"] = int(time.time())
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 获取商场列表协议
@csrf_exempt  
def get_market_list(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			time = req["time"]
			# 下面的文件记录了最近一次修改商场表的时间戳，该功能无法直接通过修改数据表里商场记录的修改时间来实现，因为商场可能被删除。此处要求后台，在进行商场修改时，一定要更新此文件。
			# 打开记录文件，如果不存在则新建
			myfile = open("/htdocs/ikea/media/config/market_update.config","a+")
			line = myfile.readline().strip()
			# 若配置文件为空，或者配置文件中的时间戳大于客户端发送过来的时间戳，则返回完整的商场列表信息，否则返回空列表
			ret_list = list()
			logging.warning("line:%s" %line)
			if line == "" or int(line) > time:				
				markets = Markets.objects.all()			
				for market in markets:
					market_dict = dict()
					market_dict["marketid"]= market.marketid
					market_dict["marketname"] = market.marketname
					ret_list.append(market_dict)
			res = json.dumps(ret_list).decode("unicode-escape")
			logging.warning("res: %s" %res)
			return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 登录协议
@csrf_exempt  
def login(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			username = req["username"]
			password = req["password"]
			marketid = req["marketid"]
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			users = Users.objects.filter(username=username)
			if users.count() > 0:
				user = users[0]
				if user.password == password:
					if user.permissionid.permissionid != 1:						
						if user.marketid.marketid != marketid:
							retdict["errno"] = errno.R_MARKET_NOT_MATCH
							res = json.dumps(retdict).decode("unicode-escape")
							logging.warning("res: %s" %res)
							return HttpResponse(res)
					retdict["permissions"] = user.permissionid.permissionid
				else:
					retdict["errno"] = errno.R_PASSWORD_ERROR
			else:
				retdict["errno"] = errno.R_USERNAME_NOT_FOUND
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 下载问卷协议
@csrf_exempt  
def get_questionnaire(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			surveyid = req["surveyid"]
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			surveys = Surveys.objects.filter(marketid=marketid).order_by('-surveyid')
			if surveys.count() > 0:
				survey = surveys[0]
				if surveyid != survey.surveyid:
					retdict["surveyid"] = survey.surveyid
					questions = list()
					questionids = json.loads(survey.questionnaireid.questionids)
					for questionid in questionids:
						question = Questions.objects.get(questionid = questionid)
						question_dict = dict()
						question_dict["question"] = question.question
						question_dict["answers"] = json.loads(question.answers)
						question_dict["choicetype"] = question.choicetype
						questions.append(question_dict)		
					retdict["questions"] = questions
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 提交问卷协议
@csrf_exempt  
def submit_questionnaire(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			surveyid = req["surveyid"]
			answers = str(req["answers"])
			answers = json.loads(answers)
			surveys = Surveys.objects.filter(surveyid=surveyid)
			if surveys.count() > 0:
				survey = surveys[0]
				result = json.loads(survey.result)
				# 验证题目个数是否匹配：
				if len(answers) != len(result):
					#logging.warning("请求提交的题目个数不匹配")
					retdict["errno"] = errno.R_QUESTIONS_NUMBER_ERROR
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)
				# 验证每个题目提交的答案序号，是否超过当前题目的答案个数：
				questions_count = len(answers)
				for index in range(0,questions_count):
					max_answer_order_num = len(result[index])
					#logging.warning("max_answer_order_num:%s" %max_answer_order_num)					
					for answer_order_num in answers[index]:
						if answer_order_num > max_answer_order_num:
							retdict["errno"] = errno.R_ANSWER_OUTOFBOUNDS_ERROR
							res = json.dumps(retdict).decode("unicode-escape")
							logging.warning("res: %s" %res)
							return HttpResponse(res)
					# 验证题目是单复选类型是否正确
					questionids = json.loads(survey.questionnaireid.questionids)
					questionid = questionids[index]
					question = Questions.objects.get(questionid = questionid)
					choicetype = question.choicetype
					#logging.warning("questiontypeid:%s    answers_size:%s"  %(choicetype,len(answers[index])))
					if choicetype == 1 and len(answers[index]) > 1:		# 如果是单选，但是提交的题目的答案个数超过1个则为错误
						retdict["errno"] = errno.R_ANSWER_TYPE_ERROR
						res = json.dumps(retdict).decode("unicode-escape")
						logging.warning("res: %s" %res)
						return HttpResponse(res)
				# 当所有验证都通过时，下面来更新统计结果：
				logging.warning("before update result:%s" %json.dumps(result).decode("unicode-escape"))
				for index in range(0,questions_count):
					for answer_order_num in answers[index]:
						result[index][answer_order_num-1] = result[index][answer_order_num-1] + 1
				logging.warning("after update result:%s" %json.dumps(result).decode("unicode-escape"))
				Surveys.objects.filter(surveyid=surveyid).update(result=result)
			else:
				retdict["errno"] = errno.R_SURVEYID_NOT_FOUND 
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 获取活动协议
@csrf_exempt  
def get_activity(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			activityid = req["activityid"]
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 取数据表中最新的一条活动记录，判断其活动时间范围是否包含今天，若包含，则为当前活动，若不包含，则无活动。
			# 若为当前活动，再判断是否与客户端请求的活动ID相同，若不同，返回新的活动信息，若相同，返回空字典。
			activities = Activities.objects.filter(marketid=marketid).order_by('-activityid')
			if activities.count() > 0:
				activity = activities[0]
				# 活动的起始与结束时间精确到天，后台需注意。
				now = date.today()
				if activity.begintime <= now and activity.endtime >= now:
					if activity.activityid != activityid:
						retdict["activityid"] = activity.activityid
						retdict["content"] = activity.content
						imagename_list = json.loads(activity.imagenames)
						url_list = list()
						for imagename in imagename_list:
							url_list.append("/htdocs/ikea/media/activity/" + imagename)
						retdict["urls"] = url_list
						retdict["begintime"] = str(activity.begintime)
						retdict["endtime"] = str(activity.endtime)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 获取配件价格协议
@csrf_exempt  
def get_unit_price(request):
	retdict = dict()	
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			time = req["time"]
			units = Units.objects.filter(updatetime__gte = time)	
			ret_list = list()
			for unit in units:
				unit_list = list()
				#unit_list.append(str(unit.unitid))
				unit_list.append(unit.unitid)
				unit_list.append(unit.price)
				ret_list.append(unit_list)
			res = json.dumps(ret_list).decode("unicode-escape")	
			logging.warning("res: %s" %res)
			return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 发送邮件协议
@csrf_exempt  
def send_email(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			email = req["email"]
			# 使用正则校验邮箱格式
			check_email_format = bool(re.match(r"[^\._-][\w\.-]+@(?:[A-Za-z0-9]+\.)+[A-Za-z]+", email,re.VERBOSE));  
			if check_email_format == False:
				retdict["errno"] = errno.R_EMAIL_ERROR
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			upload_dir = "/htdocs/ikea/media/email/"
			"""
			def handle_uploaded_file(file):
				destination = open(upload_dir + file.name, 'wb')
				for chunk in file.chunks():
					destination.write(chunk)
			   	destination.close()
			"""
			try:
				file = request.FILES['filename']
				filename = file.name
				filename_list = filename.split('.')
				if filename.split('.')[-1]  != 'png' :
					retdict["errno"] = errno.R_FILE_SUFFIX_ERROR
				else:
					handle_uploaded_file(file, upload_dir, file.name)
					# 判断邮件发送是否成功，但这里未做其它处理。因邮件可能被屏蔽等原因导致的发送失败，可酌情添加处理，如把未发送成功的邮件记录到一个地方，由人工来发。
					markets = Markets.objects.filter(marketid=marketid)
					if markets.count() < 1:
						retdict["errno"] = errno.R_MARKETID_NOT_FOUND
						res = json.dumps(retdict).decode("unicode-escape")
						logging.warning("res: %s" %res)
						return HttpResponse(res)
					tel = markets[0].tel
					if send_mail_fun(email, filename, tel):
						logging.warning("send email ok")
					else:
						logging.warning("send email faild")
			except Exception,e:
				logging.warning("Exception:%s" %e)
				retdict["errno"] = errno.R_PARAM_ERROR
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 获取组合方案（一字柜）协议
@csrf_exempt  
def get_default_design_a(request):
	retdict = dict()
	retlist = list()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			time = req["time"]
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 组合方案后台不可删除，但是有活跃与非活跃状态，只有活跃状态的组合方案才有可能发送到客户端。
			# 查询满足商场ID条件下的组合方案中（不管是否活跃，因为活跃组合改成非活跃后，也需要更新客户端），是否有修改时间比客户端请求的时间更新，只要有一个更新，则将本商场所有活跃的组合方案信息发送给客户端。
			# 客户端应做简单优化，判断只有本地没有的配置文件才重新下载，因为后台并不会修改配置文件，只可能修改简介图片而已。
			if Default_design_a.objects.filter(marketid=marketid,updatetime__gte = time).count() > 0:
				designs = Default_design_a.objects.filter(marketid=marketid,show=1).order_by("-designid")
				for design in designs:
					design_dict = dict()
					design_dict["designid"] = design.designid
					design_dict["designname"] = design.designname1
					design_dict["designdesc"] = json.loads(design.designdesc)
					design_dict["need_damping"] = design.need_damping
					design_dict["door_type"] = design.door_type
					design_dict["x"] = design.x
					design_dict["y"] = design.y
					design_dict["z"] = design.z
					design_dict["price"] = design.price
					retlist.append(design_dict)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
			res = json.dumps(retdict).decode("unicode-escape")
			logging.warning("res: %s" %res)
			return HttpResponse(res)
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
		res = json.dumps(retdict).decode("unicode-escape")
		logging.warning("res: %s" %res)
		return HttpResponse(res)
	res = json.dumps(retlist).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 获取组合方案（转角柜）协议
@csrf_exempt  
def get_default_design_b(request):
	retdict = dict()
	retlist = list()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			time = req["time"]
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 组合方案后台不可删除，但是有活跃与非活跃状态，只有活跃状态的组合方案才有可能发送到客户端。
			# 查询满足商场ID条件下的组合方案中（不管是否活跃，因为活跃组合改成非活跃后，也需要更新客户端），是否有修改时间比客户端请求的时间更新，只要有一个更新，则将本商场所有活跃的组合方案信息发送给客户端。
			# 客户端应做简单优化，判断只有本地没有的配置文件才重新下载，因为后台并不会修改配置文件，只可能修改简介图片而已。
			if Default_design_b.objects.filter(marketid=marketid,updatetime__gte = time).count() > 0:
				designs = Default_design_b.objects.filter(marketid=marketid,show=1).order_by("-designid")
				for design in designs:
					design_dict = dict()
					design_dict["designid"] = design.designid
					design_dict["designname"] = design.designname1
					design_dict["designdesc"] = json.loads(design.designdesc)
					design_dict["need_damping"] = design.need_damping
					design_dict["door_type"] = design.door_type
					design_dict["lx"] = design.lx
					design_dict["rx"] = design.rx
					design_dict["y"] = design.y
					design_dict["lz"] = design.lz
					design_dict["rz"] = design.rz
					design_dict["price"] = design.price
					retlist.append(design_dict)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
			return HttpResponse(json.dumps(retdict).decode("unicode-escape"))
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
		res = json.dumps(retdict).decode("unicode-escape")
		logging.warning("res: %s" %res)
		return HttpResponse(res)
	res = json.dumps(retlist).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 添加和修改组合方案（一字柜）协议
@csrf_exempt  
def add_default_design_a(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			base_designid = req["base_designid"]
			need_damping = req["need_damping"]
			door_type = req["door_type"]
			x = req["x"]
			y = req["y"]
			z = req["z"]
			price = req["price"]
			time = req["time"]
			# 校验上传的组合方案配置文件
			jsonfile = request.FILES['jsonfile']
			jsonfilename = jsonfile.name
			if jsonfilename.split('.')[-1]  != 'json' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 校验上传的组合方案内部结构图
			pngfile = request.FILES['pngfile']
			pngfilename = pngfile.name
			if pngfilename.split('.')[-1]  != 'png' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)			
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 判断基组合方案ID是否存在，若存在，则将基组合方案置为非活跃
			if base_designid != 0:				
				base_designs = Default_design_a.objects.filter(marketid=marketid,designid=base_designid)
				if base_designs.count() > 0:
					base_design = base_designs[0]
					base_design.show = 0
					base_design.save()
				else:
					retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)				
			# 写入新组合方案记录到DB
			new_design = Default_design_a(base_designid = base_designid,designdesc='["","","","","",""]',need_damping=need_damping,door_type=door_type,x=x,y=y,z=z,price=price,marketid=marketid,show=1,clickcount=0,buycount=0,updatetime=time)
			new_design.save()			
			# 上传文件至指定目录
			new_design_id = new_design.designid
			upload_dir = "/htdocs/ikea/media/default_design_a/"
			try:
				jsonfile = request.FILES['jsonfile']
				handle_uploaded_file(jsonfile, upload_dir, "%s.json"  %new_design_id)
				pngfile = request.FILES['pngfile']
				handle_uploaded_file(pngfile, upload_dir, "%s.png"  %new_design_id)
				retdict["designid"] = new_design_id
			except Exception,e:
				logging.warning("Exception:%s" %e)
				retdict["errno"] = errno.R_PARAM_ERROR
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 添加和修改组合方案（转角柜）协议
@csrf_exempt  
def add_default_design_b(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			base_designid = req["base_designid"]
			need_damping = req["need_damping"]
			door_type = req["door_type"]		
			lx = req["lx"]
			rx = req["rx"]
			y = req["y"]
			lz = req["lz"]
			rz = req["rz"]
			price = req["price"]
			time = req["time"]
			# 校验上传的组合方案配置文件
			jsonfile = request.FILES['jsonfile']
			jsonfilename = jsonfile.name
			if jsonfilename.split('.')[-1]  != 'json' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 校验上传的组合方案内部结构图
			pngfile = request.FILES['pngfile']
			pngfilename = pngfile.name
			if pngfilename.split('.')[-1]  != 'png' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)			
			# 判断基组合方案ID是否存在，若存在，则将基组合方案置为非活跃
			if base_designid != 0:				
				base_designs = Default_design_b.objects.filter(marketid=marketid,designid=base_designid)
				if base_designs.count() > 0:
					base_design = base_designs[0]
					base_design.show = 0
					base_design.save()
				else:
					retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)	
			# 写入新组合方案记录到DB
			new_design = Default_design_b(base_designid = base_designid,designdesc='["","","","","",""]',need_damping=need_damping,door_type=door_type,lx=lx,rx=rx,y=y,lz=lz,rz=rz,price=price,marketid=marketid,show=1,clickcount=0,buycount=0,updatetime=time)
			new_design.save()			
			# 上传文件至指定目录
			new_design_id = new_design.designid
			logging.warning("new_design_id:%s" %new_design_id)
			upload_dir = "/htdocs/ikea/media/default_design_b/"
			try:
				jsonfile = request.FILES['jsonfile']
				handle_uploaded_file(jsonfile, upload_dir, "%s.json"  %new_design_id)
				pngfile = request.FILES['pngfile']
				handle_uploaded_file(pngfile, upload_dir, "%s.png"  %new_design_id)
				retdict["designid"] = new_design_id
			except Exception,e:
				logging.warning("Exception:%s" %e)
				retdict["errno"] = errno.R_PARAM_ERROR
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 删除组合方案（一字柜）协议
@csrf_exempt  
def remove_default_design_a(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			designid = req["designid"]
			# 判断商场ID是否存在	
			designs = Default_design_a.objects.filter(designid=designid)
			if designs.count() > 0:
				design = designs[0]
				design.show = 0
				design.updatetime = int(time.time())
				design.save()
			else:
				retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 删除组合方案（转角柜）协议
@csrf_exempt  
def remove_default_design_b(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			designid = req["designid"]
			# 判断商场ID是否存在	
			designs = Default_design_b.objects.filter(designid=designid)
			if designs.count() > 0:
				design = designs[0]
				design.show = 0
				design.updatetime = int(time.time())
				design.save()
			else:
				retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 组合方案操作统计协议
@csrf_exempt  
def default_design_statistics(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			designid = req["designid"]
			designtype = req["designtype"]
			action = req["action"]			
			if designtype != "a" and designtype != "b":
					retdict["errno"] = errno.R_DESIGNTYPE_NOT_FOUND
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)
			if action != "click" and action != "buy":
					retdict["errno"] = errno.R_DESIGN_ACTION_NOT_COUNT
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)
			if designtype == "a":
				if Default_design_a.objects.filter(designid=designid).count() <1:
					retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)
			else:
				if Default_design_b.objects.filter(designid=designid).count() <1:
					retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)									
			Default_design_statistics.objects.create(designid=designid, designtype=designtype, action=action, createtime=int(time.time()))
			"""
			# 判断设计类型，是一字柜还是转角柜，然后判断统计类型，再进行相应记录到数据库
			if designtype == "a":	
				designs = Default_design_a.objects.filter(designid=designid)
				if designs.count() > 0:
					design = designs[0]
					if action == "click":
						design.clickcount = design.clickcount + 1
					else:
						design.buycount = design.buycount + 1
					design.save()
				else:
					retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)
			else:
				designs = Default_design_b.objects.filter(designid=designid)
				if designs.count() > 0:
					design = designs[0]
					if action == "click":
						design.clickcount = design.clickcount + 1
					else:
						design.buycount = design.buycount + 1
					design.save()
				else:
					retdict["errno"] = errno.R_DESIGNID_NOT_FOUND
					res = json.dumps(retdict).decode("unicode-escape")
					logging.warning("res: %s" %res)
					return HttpResponse(res)
			"""
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 添加自主设计方案（一字柜）协议
@csrf_exempt  
def add_independent_design_a(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			x = req["x"]
			y = req["y"]
			z = req["z"]
			price = req["price"]
			time = req["time"]
			file = request.FILES['filename']
			filename = file.name
			if filename.split('.')[-1]  != 'png' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				return HttpResponse(json.dumps(retdict).decode("unicode-escape"))
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)			
			# 写入新组合方案记录到DB
			new_design = Independent_design_a(x=x,y=y,z=z,price=price,marketid=marketid,createtime=time)
			new_design.save()			
			# 上传文件至指定目录
			new_design_id = new_design.designid
			upload_dir = "/htdocs/ikea/media/independent_design_a/"
			"""
			def handle_uploaded_file(file, newfilename = file.name):
				destination = open(upload_dir + newfilename, 'wb')
				for chunk in file.chunks():
					destination.write(chunk)
				destination.close()
			"""
			try:
				file = request.FILES['filename']
				handle_uploaded_file(file, upload_dir, "%s.png"  %new_design_id)
			except Exception,e:
				logging.warning("Exception:%s" %e)
				retdict["errno"] = errno.R_PARAM_ERROR
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)

# 添加自主设计方案（转角柜）协议
@csrf_exempt  
def add_independent_design_b(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			lx = req["lx"]
			rx = req["rx"]
			y = req["y"]
			lz = req["lz"]
			rz = req["rz"]
			price = req["price"]
			time = req["time"]
			file = request.FILES['filename']
			filename = file.name
			if filename.split('.')[-1]  != 'png' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)				
			# 写入新组合方案记录到DB
			new_design = Independent_design_b(lx=lx,rx=rx,y=y,lz=lz,rz=rz,price=price,marketid=marketid,createtime=time)
			new_design.save()			
			# 上传文件至指定目录
			new_design_id = new_design.designid
			upload_dir = "/htdocs/ikea/media/independent_design_b/"
			"""
			def handle_uploaded_file(file, newfilename = file.name):
				destination = open(upload_dir + newfilename, 'wb')
				for chunk in file.chunks():
					destination.write(chunk)
				destination.close()
			"""
			try:
				file = request.FILES['filename']
				handle_uploaded_file(file, upload_dir, "%s.png"  %new_design_id)
			except Exception,e:
				logging.warning("Exception:%s" %e)
				retdict["errno"] = errno.R_PARAM_ERROR
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 添加组合设计方案（一字柜）协议
@csrf_exempt  
def add_reference_design_a(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			base_designid = req["base_designid"]
			x = req["x"]
			y = req["y"]
			z = req["z"]
			price = req["price"]
			time = req["time"]
			file = request.FILES['filename']
			filename = file.name
			if filename.split('.')[-1]  != 'png' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)			
			# 写入新组合方案记录到DB
			new_design = Reference_design_a(base_designid=base_designid,x=x,y=y,z=z,price=price,marketid=marketid,createtime=time)
			new_design.save()			
			# 上传文件至指定目录
			new_design_id = new_design.designid
			upload_dir = "/htdocs/ikea/media/reference_design_a/"
			"""
			def handle_uploaded_file(file, newfilename = file.name):
				destination = open(upload_dir + newfilename, 'wb')
				for chunk in file.chunks():
					destination.write(chunk)
				destination.close()
			"""
			try:
				file = request.FILES['filename']
				handle_uploaded_file(file, upload_dir, "%s.png"  %new_design_id)
			except Exception,e:
				logging.warning("Exception:%s" %e)
				retdict["errno"] = errno.R_PARAM_ERROR
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)


# 添加组合设计方案（转角柜）协议
@csrf_exempt  
def add_reference_design_b(request):
	retdict = dict()
	if request.POST.get('req') :
		logging.warning("req: %s" %request.POST.get('req'))
		try:
			req = json.loads(request.POST.get('req'))
			marketid = req["marketid"]
			base_designid = req["base_designid"]
			lx = req["lx"]
			rx = req["rx"]
			y = req["y"]
			lz = req["lz"]
			rz = req["rz"]
			price = req["price"]
			time = req["time"]
			file = request.FILES['filename']
			filename = file.name
			if filename.split('.')[-1]  != 'png' :
				retdict["errno"] = errno.R_FILE_SUFFIX_ERROR		
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			# 判断商场ID是否存在
			if Markets.objects.filter(marketid=marketid).count() < 1:
				retdict["errno"] = errno.R_MARKETID_NOT_FOUND
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)				
			# 写入新组合方案记录到DB
			new_design = Reference_design_b(base_designid=base_designid,lx=lx,rx=rx,y=y,lz=lz,rz=rz,price=price,marketid=marketid,createtime=time)
			new_design.save()			
			# 上传文件至指定目录
			new_design_id = new_design.designid
			upload_dir = "/htdocs/ikea/media/reference_design_b/"
			"""
			def handle_uploaded_file(file, newfilename = file.name):
				destination = open(upload_dir + newfilename, 'wb')
				for chunk in file.chunks():
					destination.write(chunk)
				destination.close()
			"""
			try:
				file = request.FILES['filename']
				handle_uploaded_file(file, upload_dir, "%s.png"  %new_design_id)
			except Exception,e:
				logging.warning("Exception:%s" %e)
				retdict["errno"] = errno.R_PARAM_ERROR
				res = json.dumps(retdict).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict["errno"] = errno.R_PARAM_ERROR 
	else:
		retdict["errno"] = errno.R_PARAM_ERROR	
	res = json.dumps(retdict).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)
