#coding:utf-8
import datetime
import json
import logging
import time
from datetime import date

import MySQLdb
from django.db import connection
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

from ikea.settings import *
from models import *

import xlrd

import zipfile
import shutil
import sys
reload(sys)
sys.setdefaultencoding('utf8')  



# 上传文件，三个参数分别是文件句柄、上传目录、文件名，其中文件名默认为上传时的文件名
def handle_uploaded_file(file, upload_dir, newfilename):
	logging.warning("handle_uploaded_file:  upload_dir:%s   newfilename:%s"  %(upload_dir,newfilename))
	destination = open(upload_dir + newfilename, 'wb')
	for chunk in file.chunks():
		destination.write(chunk)
	destination.close()
	
# 从 excel 中载入配件价格数据
@csrf_exempt  
def load_units_data_handle(request):
	retdict = dict()
	retdict['status'] = 1
	logging.warning('load_units_data')
	upload_dir = "media/temp/"
	if request.method == 'POST':
		file = request.FILES['filename']
		filename = file.name
		filename_list = filename.split('.')
		if filename_list[-1] != 'xlsx' and filename_list[-1] != 'xls':
			retdict['status'] = 0
			retdict['msg'] = u'请上传EXCEL文件，后缀名为 *.xls 或 *xlsx(提示：不支持 *.%s 文件后缀)' %(filename_list[-1])
			res = json.dumps(retdict).decode("unicode-escape")
			logging.warning("res: %s" %res)
			return HttpResponse(res) 
		handle_uploaded_file(file, upload_dir, file.name)
		data = xlrd.open_workbook(upload_dir + file.name)
		sheet_name = data.sheet_names()[0]
		table = data.sheet_by_name(sheet_name)
		row_count = table.nrows
		col_count = table.ncols
		#有效行数，判断依据：如果第一列向下有一个单元格内容为空，则视为结束
		valid_row_count = 0
		for item in table.col_values(0):
			if item: 
				valid_row_count = valid_row_count+1
			else:
				break
		#有效列数，判断依据：标题行如果有一个单元格内容为空，则视为结束
		valid_col_count = 0
		for item in table.row_values(0):
			if item.strip():
				valid_col_count = valid_col_count+1
			else:
				break
		# 如果有效列数超过3，则错误
		if valid_col_count > 3:
			retdict["errno"] = "列数超过3"
			res = json.dumps(retdict).decode("unicode-escape")
			logging.warning("res: %s" %res)
			return HttpResponse(res) 
		rows = table.nrows
		cols = table.ncols
		
		logging.warning('valid_row_count:%s' %valid_row_count)
		logging.warning('valid_col_count:%s' %valid_col_count)
		
		#数据验证，验证问题类别是否在数据库中存在，验证选择类别是否是“单选”或“复选”
		for rowindex in range(1,valid_row_count):
			row = table.row_values(rowindex)
			obj = Units(row[0], row[1], row[2], '058', int(time.time()))
			obj.save()
	return HttpResponse(json.dumps(retdict))


# 载入初始化的推荐组合方案数据，并导出JSON文件
@csrf_exempt  
def load_default_design_data_handle(request):
	retdict = dict()
	retdict['status'] = 1
	logging.warning('load_default_design_data_handle')
	upload_dir = "media/temp/"
	upload_file = request.FILES['filename']
	filename = upload_file.name
	filename_list = filename.split('.')
	if filename_list[-1] != 'zip' :
		retdict['status'] = 0
		retdict['msg'] = u'请上传 .zip 文件，不支持 *.%s 文件后缀)' %(filename_list[-1])
		res = json.dumps(retdict).decode("unicode-escape")
		logging.warning("res: %s" %res)
		return HttpResponse(res)
	# 上传zip文件 
	handle_uploaded_file(upload_file, upload_dir, upload_file.name)
	zipFile = zipfile.ZipFile(upload_dir + upload_file.name, 'r')  
	# 解压zip文件
	for f in zipFile.namelist():
		zipFile.extract(f,upload_dir)
	# 解析一字柜的JSON文件
	dir1 = '%s%s/a/' %(upload_dir, upload_file.name[:-4])
	jsonfile1 = dir1 + 'data.json'
	logging.warning(jsonfile1)
	datafile1 = file(jsonfile1)
	json1 = json.load(datafile1)
	markets = Markets.objects.all();
	output_list1 = list()
	for design in json1:
		local_designid = design['local_designid']
		need_damping = design['need_damping']
		door_type = design['door_type']
		x = design['x']
		y = design['y']
		z = design['z']
		price = design['price']
		time = design['time']
		designname = design['designname']
		designdesc = json.dumps(design['designdesc']).decode("unicode-escape")
		jsonfile = design['jsonfile']
		pngfile = design['pngfile']
		coverfile = design['coverfile']
		for market in markets:
			new_design = Default_design_a(base_designid = 0, designname1=designname, designdesc=designdesc, need_damping=need_damping,door_type=door_type,x=x,y=y,z=z,price=price,marketid=market.marketid,show=1,updatetime=time)
			new_design.save()
			new_designid = new_design.designid
			tempdict = dict()
			tempdict['local_designid'] = local_designid
			tempdict['marketid'] = market.marketid
			tempdict['designid'] = new_designid
			output_list1.append(tempdict)
			shutil.copyfile('%sjsonfile/%s' %(dir1, jsonfile), 'media/default_design_a/%s.json' %new_designid)
			shutil.copyfile('%spngfile/%s' %(dir1, pngfile), 'media/default_design_a/%s.png' %new_designid)
			shutil.copyfile('%scoverfile/%s' %(dir1, coverfile), 'media/default_design_a/%s_cover.png' %new_designid)
	logging.warning('output_json1:\n%s' %json.dumps(output_list1))
	datafile1.close()
	
	# 解析转角柜的JSON文件
	dir2 = '%s%s/b/' %(upload_dir, upload_file.name[:-4])
	jsonfile2 = dir2 + 'data.json'
	logging.warning(jsonfile2)
	datafile2 = file(jsonfile2)
	json2 = json.load(datafile2)
	markets = Markets.objects.all();
	output_list2 = list()
	for design in json2:
		local_designid = design['local_designid']
		need_damping = design['need_damping']
		door_type = design['door_type']
		lx = design['lx']
		rx = design['rx']
		y = design['y']
		lz = design['lz']
		rz = design['rz']
		price = design['price']
		time = design['time']
		designname = design['designname']
		designdesc = json.dumps(design['designdesc']).decode("unicode-escape")
		jsonfile = design['jsonfile']
		pngfile = design['pngfile']
		coverfile = design['coverfile']		
		for market in markets:
			new_design = Default_design_b(base_designid = 0, designname1=designname, designdesc=designdesc, need_damping=need_damping,door_type=door_type,lx=lx,rx=rx,y=y,lz=lz,rz=rz,price=price,marketid=market.marketid,show=1,updatetime=time)
			new_design.save()
			new_designid = new_design.designid
			tempdict = dict()
			tempdict['local_designid'] = local_designid
			tempdict['marketid'] = market.marketid
			tempdict['designid'] = new_designid
			output_list2.append(tempdict)
			shutil.copyfile('%sjsonfile/%s' %(dir2, jsonfile), 'media/default_design_b/%s.json' %new_designid)
			shutil.copyfile('%spngfile/%s' %(dir2, pngfile), 'media/default_design_b/%s.png' %new_designid)
			shutil.copyfile('%scoverfile/%s' %(dir2, coverfile), 'media/default_design_b/%s_cover.png' %new_designid)
	logging.warning('output_json2:\n%s' %json.dumps(output_list2))
	datafile2.close()

	return HttpResponse('一字柜：<br />%s<br /><br />转角柜：<br />%s'  %(json.dumps(output_list1), json.dumps(output_list2)))