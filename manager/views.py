#coding:utf-8

import datetime
import json
import decimal
import logging
import time

import MySQLdb
from django.db import connection
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

from ikea.settings import *
from models import *
import xlrd
import random
import xlwt
import StringIO
import time
import urllib

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage


def send_mail(sub,content):
	logging.warning('send_mail')
	mailto_list=['532570520@163.com','532570520@qq.com','470021161@qq.com']
	#mailto_list=['532570520@163.com']
	mail_host="smtp.163.com"  #设置服务器
	mail_user="ikea_pax_planner@163.com"   #用户名
	#mail_user="tianya.juanke@163.com"   #用户名
	mail_pass="ikea2014"   #口令
	mail_postfix="163.com"  #发件箱的后缀
	me=u"宜家"+"<"+mail_user+"@"+mail_postfix+">"
	msg = MIMEText(content,_subtype='plain',_charset='gb2312')
	msg['Subject'] = sub
	msg['From'] = me
	msg['To'] = ";".join(mailto_list)
	try:
		server = smtplib.SMTP()
		server.connect(mail_host)
		server.login(mail_user,mail_pass)
		server.sendmail(me, mailto_list, msg.as_string())
		server.close()
		return True
	except Exception, e:
		print str(e)
		print 'error'
		return False

	
# 加密URLENCODE方法，用于中文字符正确显示
def urlencode(s):
	return repr(s).replace(r'\x', '%')[1:-1]

# 上传文件，三个参数分别是文件句柄、上传目录、文件名，其中文件名默认为上传时的文件名
def handle_uploaded_file(file, upload_dir, newfilename):
	logging.warning("handle_uploaded_file:  upload_dir:%s   newfilename:%s"  %(upload_dir,newfilename))
	destination = open(upload_dir + newfilename, 'wb')
	for chunk in file.chunks():
		destination.write(chunk)
	destination.close()
	

# 视图包装函数，用于校验用户是否登录，若没有登录则跳转到登录页面，登录则可继续跳转到指定视图函数
def requires_login(view):
	#logging.warning("requires_login")
	def new_view(request, *args, **kwargs):
		if not 'userid' in request.session or not 'username' in request.session or not 'marketid' in request.session or not 'permissionid' in request.session:
			logging.warning("user is not login, jump to login.html")
			return HttpResponseRedirect("/login/") 
		else:
			return view(request, *args, **kwargs)
	return new_view 


# 登录处理
def login_handle(request):
	res = dict()
	if request.POST.get('username') and request.POST.get('password') :
		try:
			username = request.POST.get('username')
			password = request.POST.get('password')
			logging.warning("req ->  username:%s    password:%s " %(username,password))
			if Users.objects.filter(username=username).count() > 0:
				users = Users.objects.filter(username=username, password=password)
				if users.count() > 0:
					user = users[0]
					res["status"] = 1
					# 保存一些常用数据到session
					request.session['userid'] = user.userid
					request.session['username'] = user.username
					request.session['marketid'] = user.marketid.marketid
					request.session['marketname'] = user.marketid.marketname
					request.session['permissionid'] = user.permissionid.permissionid
				else:
					res["status"] = 0
					res["msg"] = "密码不正确"						
			else:
				logging.warning("username:%s  password:%s" %(username,password))
				res["status"] = 0
				res["msg"] = "帐号不存在"
		except Exception,e:
			logging.warning("Exception:%s" %e)
			res["status"] = 0
			res["msg"] = "服务器错误，请稍候重试。"	
	else:
		logging.warning("表单提交错误！")
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 修改密码
def update_pwd_handle(request):	
	res = dict()
	if request.POST.get('newpwd') and request.POST.get('newpwdrepeat'):
		newpwd = request.POST.get('newpwd')
		newpwdrepeat = request.POST.get('newpwdrepeat')
		logging.warning("req ->  newpwd:%s    newpwdrepeat:%s " %(newpwd,newpwdrepeat))
		if(newpwd == newpwdrepeat):	
			Users.objects.filter(userid=request.session['userid']).update(password=newpwd)
			res["status"] = 1
		else:
			res["status"] = 0
			res["msg"] = "两次密码输入不一致！" 
	else:
		logging.warning("表单提交错误！")
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 进入主页
def index(request):
	username = str(request.session['username'])
	return render_to_response("index.html",{"username" : str(request.session['username']), "permissionid": request.session['permissionid'], 'marketid': request.session['marketid']})


# 注销
def logout(request):
	del request.session['userid']
	del request.session['username']
	del request.session['marketid']
	del request.session['permissionid']
	return HttpResponseRedirect("/login/")


# 获取商场列表
def markets_data(request):
	res = dict()
	rowslist = list()
	cur = connection.cursor()
	sql = "SELECT a.marketid,a.marketname,a.tel,b.usercount FROM markets a LEFT JOIN (SELECT marketid,SUM(1) AS usercount FROM users GROUP BY marketid) b ON a.marketid = b.marketid ORDER BY a.marketid"
	if request.POST.get('sort') and request.POST.get('order'):		
		sql = "SELECT a.marketid,a.marketname,a.tel,b.usercount FROM markets a LEFT JOIN (SELECT marketid,SUM(1) AS usercount FROM users GROUP BY marketid) b ON a.marketid = b.marketid order by %s %s" %(request.POST.get('sort'),request.POST.get('order'))
	logging.warning("sql:%s" %sql)
	cur.execute(sql)
	data = cur.fetchall()
	if data:
		for row in data:
			itemdict = dict()
			itemdict['marketid'] = str(row[0])
			itemdict['marketname'] = str(row[1])
			itemdict['tel'] = row[2] if row[2] else ""
			itemdict['usercount'] = 0 if row[3]==None else int(row[3])
			rowslist.append(itemdict)
		res['rows'] = rowslist
		res['total'] = len(rowslist)
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 获取用户数据
def users_data(request):
	res = dict()
	rowslist = list()
	cur = connection.cursor()
	sql = "SELECT users.userid,users.username,users.`name`,markets.marketid,markets.marketname,permissions.permission FROM users,permissions,markets WHERE users.permissionid = permissions.permissionid AND users.marketid = markets.marketid"
	if 'marketid' in request.POST:
		logging.warning("req ->  marketid:%s" %request.POST["marketid"])
		marketid = request.REQUEST.get("marketid")
	else:
		marketid = request.session["marketid"]
	sql = "SELECT users.userid,users.username,users.`name`,markets.marketid,markets.marketname,permissions.permission FROM users,permissions,markets WHERE users.permissionid = permissions.permissionid AND users.marketid = markets.marketid AND  users.marketid = '%s'" %marketid
	if request.POST.get('sort') and request.POST.get('order'):
		logging.warning("req ->  sort:%s  order:%s" %(request.POST["sort"],request.POST["order"]))
		sql += " order by %s %s" %(request.POST.get('sort'),request.POST.get('order'))
	else:
		sql += " order by users.userid desc"
	logging.warning('sql:%s' %sql)
	cur.execute(sql)
	data = cur.fetchall()
	if data:
		for row in data:
			itemdict = dict()	
			itemdict['userid'] = int(row[0])	
			itemdict['username'] = str(row[1])
			itemdict['name'] = str(row[2])
			#itemdict['marketname'] = '%s-%s' %(row[3],row[4])
			itemdict['marketname'] = str(row[4])
			itemdict['permission'] = str(row[5])
			rowslist.append(itemdict)
	res['rows'] = rowslist
	res['total'] = len(rowslist)
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 保存商场数据
def market_save(request):
	res = dict()
	res['status'] = 0
	logging.warning("market save")
	if request.POST.get('marketid_new') and request.POST.get('marketname') and request.POST.get('tel'):
		marketid_new = request.POST.get('marketid_new')
		marketname = request.POST.get('marketname')
		tel = request.POST.get('tel')
		try:
			if request.POST.get('marketid_old'):
				# 修改商场信息
				"""
				if Markets.objects.filter(marketid=marketid_new).count() > 0 or Markets.objects.filter(marketname=marketname).count() > 0:
					res['msg'] = '保存失败，请检查商场编号是否重复。'
					res = json.dumps(res).decode("unicode-escape")
					logging.warning("res ->  %s " %res)
					return HttpResponse(res)
				"""
				marketid_old = request.POST.get('marketid_old')
				if marketid_old != marketid_new:
					# 修改后的商场ID符合条件，检查旧的商场ID是否被用到，如果被其它表用到，不可以删除（使用外键表，删除会直接报异常）
					try:
						Markets.objects.filter(marketid=marketid_old).update(marketid=marketid_new, marketname=marketname,tel=tel)
						res['status'] = 1
					except Exception:
						res['msg'] = '本商场编号在其它数据中有使用或商场编号重复，不允许被修改或删除。'
						res = json.dumps(res).decode("unicode-escape")
						logging.warning("res ->  %s " %res)
						return HttpResponse(res)
				else:					
					Markets.objects.filter(marketid=marketid_old).update(marketname=marketname,tel=tel)
					res['status'] = 1
			else:
				logging.warning("add market")
				# 添加新商场，先校验配件价格模板文件
				data = xlrd.open_workbook("/htdocs/ikea/media/units/units.xlsx")
				sheet_name = data.sheet_names()[0]
				table = data.sheet_by_name(sheet_name)
				row_count = table.nrows
				col_count = table.ncols
				#有效行数，判断依据：如果第一列向下有一个单元格内容为空，则视为结束
				valid_row_count = 0
				for item in table.col_values(0):
					if item: 
						valid_row_count = valid_row_count+1
					else:
						break
				#有效列数，判断依据：标题行如果有一个单元格内容为空，则视为结束
				valid_col_count = 0
				for item in table.row_values(0):
					if item:
						valid_col_count = valid_col_count+1
					else:
						break
				# 如果有效列数超过3，则错误
				if valid_col_count > 3:
					logging.warning('有效列数超过3')
					res["status"] = 0
					res["msg"] = "配件价格模板文件错误，有效列数超过3"
					res = json.dumps(res).decode("unicode-escape")
					logging.warning("res: %s" %res)
					# 无法导入配件数据，则中止商场新建
					return HttpResponse(res) 
				rows = table.nrows
				cols = table.ncols
				logging.warning('valid_row_count:%s' %valid_row_count)
				logging.warning('valid_col_count:%s' %valid_col_count)
				# 配件价格模板校验OK后，添加新商场，然后继续添加该商场的配件价格数据：
				Markets.objects.create(marketid=marketid_new, marketname=marketname,tel=tel)
				# 若有符合该商场的配件价格信息，先删除：
				Units.objects.filter(marketid=marketid_new).delete()
				# 遍历写入配件价格信息
				for rowindex in range(1,valid_row_count):
					row = table.row_values(rowindex)
					logging.warning("price:%s"  %row[2])
					Units.objects.create(unitid=row[0], unitname=row[1], price=row[2], marketid=marketid_new, updatetime=int(time.time()))	
				res['status'] = 1	
		except Exception,e:
			logging.warning("Exception:%s" %e)
			res['msg'] = '请检查商场编号或商场名称是否有重复。'
	else:
		res['msg'] = '提交请求参数错误，请检查。'
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 保存用户数据
def user_save(request):
	logging.warning('usre_save')
	retdict = dict()
	retdict['status'] = 1
	if request.POST.get('username') and request.POST.get('name') and request.POST.get('marketid') and request.POST.get('permissionid'):
		username = request.POST.get('username')
		name = request.POST.get('name')
		marketid = request.POST.get('marketid')
		logging.warning('marketid:%s' %marketid)
		permissionid = int(request.POST.get('permissionid'))
		self_userid = request.session['userid']
		self_user = Users.objects.get(userid=self_userid)
		#try:
		if request.POST.get('userid'):
			# 修改用户信息
			userid = request.POST.get('userid')
			logging.warning(len(Users.objects.filter(username = username).exclude(userid = userid)))
			if len(Users.objects.filter(username = username).exclude(userid = userid)) == 0:
				user = Users.objects.filter(userid=userid)[0]
				Users.objects.filter(userid = userid).update(username=username, name=name, marketid=marketid, permissionid=permissionid)
				market = Markets.objects.filter(marketid=marketid)[0]		
				send_mail(u'编辑帐号提醒', u'%s %s 员工 %s 编辑 账号 %s' %(self_user.marketid.marketid, self_user.marketid.marketname.decode("utf-8"), self_user.username, user.username))
			else:
				retdict['status'] = 0
				retdict['msg'] = '保存失败，请检查帐号是否重复。'
		else:
			# 添加新用户
			if Users.objects.filter(username = username).count() == 0: 
				market = Markets.objects.get(marketid=marketid)
				permission = Permissions.objects.get(permissionid=permissionid)
				Users.objects.create(username=username, password='123456', name=name, marketid=market,permissionid=permission)
				content = u'%s %s 员工 %s 新增 账号 %s' %(self_user.marketid.marketid, self_user.marketid.marketname.decode("utf-8"), self_user.username, username)
				send_mail(u'新增帐号提醒', content)
			else:
				retdict['status'] = 0
				retdict['msg'] = '保存失败，请检查帐号是否重复。'
		"""					
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict['status'] = 0
			retdict['msg'] = '保存失败，请检查帐号是否重复。'
		"""			
	else:
		retdict['status'] = 0
		retdict['msg'] = '提交数据错误，请检查。'
	return HttpResponse(json.dumps(retdict).decode("unicode-escape"))


# 删除指定商场
def market_remove(request):
	res = dict()
	res['status'] = 0
	if request.POST.get('marketid'):
		marketid = request.POST.get('marketid')
		logging.warning("req ->  marketid:%s" %marketid)
		cur = connection.cursor()
		sql = "delete from markets where marketid = '%s'" %marketid
		try:
			cur.execute(sql)
			data = cur.fetchone()
			# 成功删除商场后，清空商场配件价格信息：
			Units.objects.filter(marketid=marketid).delete()
			res['status'] = 1
		except Exception:
			res['msg'] =  '本商场编号在其它数据中有使用，不允许被修改或删除。'
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 删除指定用户
def user_remove(request):
	res = dict()
	res['status'] = 0
	if request.POST.get('userid'):
		try:
			objs = Users.objects.filter(userid = request.POST.get('userid'))
			if objs.count() > 0:
				obj = objs[0]
				username = obj.username
				self_userid = request.session['userid']
				self_user = Users.objects.get(userid=self_userid)
				send_mail(u'删除帐号提醒', u'%s %s 员工 %s 删除 账号 %s' %(self_user.marketid.marketid, self_user.marketid.marketname.decode("utf-8"), self_user.username, username))
				Users.objects.filter(userid=request.POST.get('userid')).delete()
				res['status'] = 1
		except Exception:
			res['msg'] = '数据库操作失败，请稍候重试。'
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 获取用户列表中编辑状态下的“所属商场”下拉列表的数据接口
def market_list(request):
	res = list()
	retdict = dict()	
	cur = connection.cursor()
	sql = "select marketid,marketname from markets"
	if request.session['permissionid'] == 2:
		sql = "select marketid,marketname from markets where marketid='%s'" %request.session['marketid']
	try:
		cur.execute(sql)
		data = cur.fetchall()
		if data:
			for row in data:
				retdict = dict()
				retdict["market_value"] = str(row[0])
				#retdict["marketname"] = '%s-%s' %(row[0], row[1])
				retdict["market_text"] = str(row[1])
				res.append(retdict)
	except Exception,e:
		res['msg'] = e
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 获取商场列表数据，带有额外的 【all->'全国'】 的数据项
def market_list_with_all(request):
	res = list()
	retdict = dict()	
	cur = connection.cursor()
	sql = "select marketid,marketname from markets"
	try:
		cur.execute(sql)
		data = cur.fetchall()
		if data:
			all_market_dict = dict()
			all_market_dict["market_value"] = "all"
			all_market_dict["market_text"] = "全国"
			res.append(all_market_dict)
			for row in data:
				retdict = dict()
				retdict["market_value"] = str(row[0])
				#retdict["marketname"] = '%s-%s' %(row[0], row[1])
				retdict["market_text"] = str(row[1])
				res.append(retdict)
	except Exception,e:
		res['msg'] = e
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 获取用户列表中编辑状态下的“权限”下拉列表的数据接口
def permission_list(request):
	res = list()
	retdict = dict()	
	cur = connection.cursor()
	sql = "select permissionid,permission from permissions"
	if request.session['permissionid'] == 2:
		sql = "select permissionid,permission from permissions where permissionid>=3"
	if request.session['permissionid'] == 1:
		sql = "select permissionid,permission from permissions where permissionid>=2"
	try:
		cur.execute(sql)
		logging.warning(sql)
		data = cur.fetchall()
		if data:
			for row in data:
				retdict = dict()
				retdict["permissionid"] = int(row[0])
				retdict["permission"] = row[1]
				res.append(retdict)
	except Exception,e:
		res['msg'] = e
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 问题类别数据
def questiontype_data(request):	
	res = dict()
	rowslist = list()		
	objs = QuestionType.objects.all()
	logging.warning(len(objs))
	if objs:
		for row in objs:
			itemdict = dict()
			itemdict['questiontypeid'] = int(row.questiontypeid)
			itemdict['questiontype'] = str(row.questiontype)
			rowslist.append(itemdict)
		res['rows'] = rowslist
		res['total'] = len(rowslist)
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


#  问题类别保存
def questiontype_save(request):	
	res = dict()
	res['status'] = 1	
	if request.POST.get('questiontype'):
		try:
			if 'questiontypeid' in request.POST:
				QuestionType.objects.filter(questiontypeid=int(request.POST.get('questiontypeid'))).update(questiontype=request.POST.get('questiontype'))
			else:
				QuestionType.objects.create(questiontype=request.POST.get('questiontype'))
		except Exception:
			res['status'] = 0
			res['msg'] = '数据库操作失败！请重试。'
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 问题类别删除
def questiontype_remove(request):	
	res = dict()
	res['status'] = 1	
	if request.POST.get('questiontypeid'):
		try:
			objs = Questions.objects.filter(questiontypeid = int(request.POST.get('questiontypeid')))
			if len(objs) > 0:
				res['status'] = 0
				res['msg'] = '已经有问题使用到了该类别，故不能删除！'
			else:
				QuestionType.objects.filter(questiontypeid=int(request.POST.get('questiontypeid'))).delete()
		except Exception:
			res['status'] = 0
			res['msg'] = '数据库操作失败！请重试。'
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 题目列表页面中问题类型下拉列表数据
def questiontype_list(request):	
	res = list()
	retdict = dict()	
	objs = QuestionType.objects.all()
	for obj in objs:
		retdict = dict()
		retdict["questiontypeid"] = obj.questiontypeid
		retdict["questiontype"] = obj.questiontype
		res.append(retdict)
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 题目数据
def questions_data(request):
	res = dict()
	rowslist = list()	
	min = 0
	max = 0
	if request.POST.get('page') and request.POST.get('rows'):
		page = int(request.POST.get('page'))
		rows = int(request.POST.get('rows'))
		min = (page-1) * rows
		max = page * rows
	#logging.warning("%s-%s" %(min,max))
	objs = Questions.objects.all()[min:max]
	total = Questions.objects.count()
	# 下面处理模糊查寻：
	if request.REQUEST.get('question_like'):
		question_like = request.REQUEST.get('question_like')
		objs = Questions.objects.filter(question__contains=question_like)[min:max]	
		total = Questions.objects.filter(question__contains=question_like).count()
	logging.warning(objs.count())
	# 下面注意，必须保证objs.count() > 0 后才能使用后面的for循环，否则一旦objs.count()==0,则for循环会出错，而不是略过。
	if objs.count() > 0:
		for obj in objs:
			#logging.warning(objs.count())
			itemdict = dict()
			itemdict['questionid'] = obj.questionid
			itemdict['question'] = obj.question
			answers =  json.loads(obj.answers)
			itemdict['answers_json'] = answers
			answers_format = str()
			index = 0
			for item in answers:
				index = index + 1
				answers_format += '%s. %s<br />' %(index,item)
			itemdict['answers'] = answers_format
			itemdict['choicetype'] = obj.choicetype
			itemdict['questiontypeid'] = obj.questiontypeid.questiontypeid
			itemdict['questiontype'] = obj.questiontypeid.questiontype
			itemdict['createtime'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(obj.createtime)))
			rowslist.append(itemdict)
	res['rows'] = rowslist
	res['total'] = total
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 增加题目
def question_add(request):
	retdict = dict()
	retdict['status'] = 0
	retdict['msg'] = '数据库错误，请稍候重试。'
	if request.POST.get('question') and request.POST.get('answers') and request.POST.get('questiontypeid') and request.POST.get('choicetype'):
		question =  request.POST.get('question')
		answer_list = request.POST.getlist("answers")
		#answer_list.reverse()
		answers = list()
		for answer in answer_list:
			answers.append(answer)
		questiontypeid = request.POST.get('questiontypeid')
		choicetype = request.POST.get('choicetype')
		
		obj = Questions()
		obj.question = question
		obj.answers = json.dumps(answers).decode("unicode-escape")
		obj.questiontypeid = QuestionType.objects.get(questiontypeid = questiontypeid)
		obj.choicetype = choicetype
		obj.createtime = int(time.time())
		obj.save()	
		
		retdict['status'] = 1
	return HttpResponse(json.dumps(retdict).decode("unicode-escape"))	


# 删除题目
def question_remove(request):
	retdict = dict()
	retdict['status'] = 0
	retdict['msg'] = '数据库错误，请稍候重试。'
	if request.POST.get('questionids'):
		logging.warning(request.POST.get('questionids'))
		# 检测待删除的问题有没有被问卷使用过：		
		for questionid in json.loads(request.POST.get('questionids')):
			logging.warning('questionid:%s' %questionid)
			sql = "SELECT count(*) FROM questionnaires WHERE REPLACE(REPLACE(REPLACE(questionids,'[',','),']',','),' ','') LIKE '%%,%s,%%'" %questionid
			logging.warning('sql:%s' %sql)
			cur = connection.cursor()
			cur.execute(sql)
			logging.warning('questionid:%s' %questionid)
			data = cur.fetchone()
			logging.warning('questionid:%s' %questionid)
			if data[0] > 0:
				logging.warning(data[0])
				retdict['status'] = 0
				retdict['msg'] = '所选题目正在被问卷使用，不可删除。'
				return HttpResponse(json.dumps(retdict).decode("unicode-escape"))
		# 依次删除题目	
		for questionid in json.loads(request.POST.get('questionids')):	
			obj = Questions.objects.get(questionid = questionid)
			obj.delete()
		retdict['status'] = 1
	return HttpResponse(json.dumps(retdict).decode("unicode-escape"))	


# 上传题目的EXCEL文件
def upload_questions(request):
	retdict = dict()
	retdict['status'] = 1
	logging.warning('upload_questions')
	upload_dir = "/htdocs/ikea/media/questions/"
	def handle_uploaded_file(file):
		logging.warning('handle_uploaded_file')
		destination = open(upload_dir + file.name, 'wb')
		for chunk in file.chunks():
			destination.write(chunk)
	   	destination.close()
	if request.method == 'POST':
		file = request.FILES['filename']
		filename = file.name
		filename_list = filename.split('.')
		if filename_list[-1] != 'xlsx' and filename_list[-1] != 'xls':
			retdict['status'] = 0
			retdict['msg'] = u'请上传EXCEL文件，后缀名为 *.xls 或 *xlsx(提示：不支持 *.%s 文件后缀)' %(filename_list[-1])
			return HttpResponse(json.dumps(retdict).decode("unicode-escape"))			 
		handle_uploaded_file(file)
		logging.warning('000')
		#xlrd.Book.encoding = 'gbk'
		data = xlrd.open_workbook(upload_dir + file.name)
		sheet_name = data.sheet_names()[0]
		table = data.sheet_by_name(sheet_name)
		row_count = table.nrows
		col_count = table.ncols
		logging.warning('111')
		#有效行数，判断依据：如果第一列向下有一个单元格内容为空，则视为结束
		valid_row_count = 0
		for item in table.col_values(0):
			if item: 
				valid_row_count = valid_row_count+1
			else:
				break
		logging.warning('222')
		if valid_row_count < 1:
			retdict['status'] = 0
			retdict['msg'] = u'EXCEL格式错误，请修正后再上传。'
			return HttpResponse(json.dumps(retdict).decode("unicode-escape"))		
		#有效列数，判断依据：标题行如果有一个单元格内容为空，则视为结束
		valid_col_count = 0
		for item in table.row_values(0):
			if item:
				valid_col_count = valid_col_count+1
			else:
				break
		logging.warning('333')
		rows = table.nrows
		cols = table.ncols
		
		logging.warning('valid_row_count:%s' %valid_row_count)
		logging.warning('valid_col_count:%s' %valid_col_count)
		
		if valid_col_count < 5:
			retdict['status'] = 0
			retdict['msg'] = u'EXCEL格式错误，请修正后再上传。'
			return HttpResponse(json.dumps(retdict).decode("unicode-escape"))				
		
		#数据验证，验证问题类别是否在数据库中存在，验证选择类别是否是“单选”或“复选”
		try:
			questions_list= list()
			for rowindex in range(1,valid_row_count):
				question_dict = dict()
				row = table.row_values(rowindex)
				question_dict['question'] = row[0]
				question_dict['questiontype'] = row[1]
				if row[2] != u'单选' and row[2] != u'复选':
					retdict['status'] = 0
					retdict['msg'] = u'EXCEL格式错误，请修正后再上传。(提示：第 %s 行选择类型【 %s】不是单选或复选)' %(rowindex+1,row[2])
					return HttpResponse(json.dumps(retdict).decode("unicode-escape"))	
				question_dict['choicetype'] = 1 if row[2] == u'单选' else 2
				answers_list = list()		
				for colindex in range(3,valid_col_count):				
					if(row[colindex]):
						answers_list.append(row[colindex])
					else:
						break
				question_dict['answers'] = answers_list
				logging.warning(json.dumps(answers_list).decode('unicode-escape'))
				questions_list.append(question_dict)
			objs = QuestionType.objects.all()
			questiontype_list = list()
			for obj in objs:
				questiontype_list.append(obj.questiontype)
			for index in range(0,len(questions_list)):
				if questions_list[index]['questiontype'].encode('utf-8') not in questiontype_list:
					retdict['status'] = 0
					retdict['msg'] = u'EXCEL格式错误，请修正后再上传。(提示：第 %s 行问题类型【%s】不存在。)' %(index+2,questions_list[index]['questiontype'])
					return HttpResponse(json.dumps(retdict).decode("unicode-escape"))
			#所有数据校验OK，将 questions_list 中的数据写入DB
			for item in questions_list:
				obj = Questions()
				obj.question = item['question']
				obj.questiontypeid = QuestionType.objects.get(questiontype = item['questiontype'])
				obj.choicetype = item['choicetype']
				obj.answers = json.dumps(item['answers']).decode('unicode-escape')
				obj.createtime = int(time.time())
				obj.save()
		except Exception,e:
			logging.warning("Exception:%s" %e)
			retdict['status'] = 0
			retdict['msg'] = u'EXCEL格式错误，请修正后再上传。' 
			return HttpResponse(json.dumps(retdict).decode("unicode-escape"))				
	return HttpResponse(json.dumps(retdict).decode("unicode-escape"))


# 生成问卷
def questionnaire_generate(request):
	retdict = dict()
	marketid = request.session['marketid']
	if 'marketid' in request.REQUEST:
		marketid = request.REQUEST.get('marketid')
	if 'questionids' not in request.REQUEST:
		retdict['status'] = 0
		retdict['msg'] = 'questionids not found!'
		return HttpResponse(json.dumps(retdict).decode("unicode-escape"))
	markets = Markets.objects.filter(marketid=marketid)
	if markets.count() <= 0:
		retdict['status'] = 0
		retdict['msg'] = 'marketid not found!'
		return HttpResponse(json.dumps(retdict).decode("unicode-escape"))
	market = markets[0]			
	questionids = request.REQUEST.get('questionids')
	questionid_list = questionids.split(',')
	questionid_list_with_int = list()
	for questionid in questionid_list:
		questionid_list_with_int.append(int(questionid))
	newobj = Questionnaires.objects.create(questionids=json.dumps(questionid_list_with_int).decode("unicode-escape"), marketid=market, createtime=int(time.time()))
	retdict['status'] = 1
	retdict['questionnaireid'] = newobj.questionnaireid
	return HttpResponse(json.dumps(retdict).decode("unicode-escape"))


# 获取问卷管理的模板页面，返回默认商场编号
def questionnaire(request):
	res = dict()
	marketid = str()
	if 'marketid' in request.REQUEST:
		marketid = request.REQUEST.get('marketid')
	else:
		marketid = request.session["marketid"]
	res["marketid"] = marketid
	surveys = Surveys.objects.filter(marketid=marketid).order_by('-surveyid')
	if surveys.count() > 0:
		survey = surveys[0]
		res["questionnaireid"] = survey.questionnaireid.questionnaireid
		res["time"] = survey.surveyid
		#res["time"] = '%s-%s' %(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(survey.begintime)), time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(survey.endtime))) if survey.endtime else "当前" )
	else:			
		res["questionnaireid"] = None
		res["time"] = None
	return render_to_response('questionnaire.html', res)


# 根据商场ID获取问卷列表
def questionnaires_by_marketid(request):
	retlist = list()
	marketid = request.session['marketid']
	if 'marketid' in request.REQUEST:
		marketid = request.REQUEST.get('marketid')
	markets = Markets.objects.filter(marketid=marketid)
	if markets.count() <= 0:
		return HttpResponse(json.dumps(retlist).decode("unicode-escape"))
	market = markets[0]		
	objs = Questionnaires.objects.filter(marketid=market)
	if objs.count() > 0:
		for obj in objs:
			retdict = dict()
			retdict['questionnaire_text'] = obj.questionnaireid
			retdict['questionnaire_value'] = obj.questionnaireid
			retlist.append(retdict)	
	return HttpResponse(json.dumps(retlist).decode("unicode-escape"))


# 根据问卷ID获取问卷活动列表
def time_by_questionnaireid(request):
	retlist = list()
	questionnaireid = int()
	if 'questionnaireid' in request.REQUEST:
		questionnaireid = int(request.REQUEST.get('questionnaireid'))
		questionnaires = Questionnaires.objects.filter(questionnaireid=questionnaireid)
		if questionnaires.count() > 0:
			questionnaire = questionnaires[0]
			logging.warning(questionnaire)
			objs = Surveys.objects.filter(questionnaireid=questionnaire)
			logging.warning(objs.count())
			if objs.count() > 0:
				for obj in objs:
					retdict = dict()
					retdict['time_text'] = '%s-%s' %(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(obj.begintime)), time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(obj.endtime))) if obj.endtime else "当前" )
					retdict['time_value'] = obj.surveyid
					retlist.append(retdict)
	else:
		marketid = request.session['marketid']
		surveys = Surveys.objects.filter(marketid=marketid).order_by('-surveyid')
		if surveys.count() > 0:
			survey = surveys[0]
			#questionnaireid = survey.questionnaireid.questionnaireid
			objs = Surveys.objects.filter(marketid=marketid, questionnaireid = survey.questionnaireid).order_by('-surveyid')
			if objs.count() > 0:
				logging.warning('00000')
				for obj in objs:
					retdict = dict()
					retdict['time_text'] = '%s-%s' %(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(obj.begintime)), time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(obj.endtime))) if obj.endtime else "当前" )
					retdict['time_value'] = obj.surveyid
					retlist.append(retdict)
	logging.warning(json.dumps(retlist).decode("unicode-escape"))
	return HttpResponse(json.dumps(retlist).decode("unicode-escape"))


# 根据问卷号或问卷活动ID获取问卷详情
def questionnaire_by_questionnaireid_or_surveyid(request):
	res = dict()
	rowslist = list()
	surveyid = 0
	if 'surveyid' in request.REQUEST:
		if request.REQUEST.get('surveyid'):
			surveyid = int(request.REQUEST.get('surveyid'))
	if 'questionnaireid' in request.REQUEST:	
		questionnaireid = int(request.REQUEST.get('questionnaireid'))
		questionnaires = Questionnaires.objects.filter(questionnaireid = questionnaireid)
		if questionnaires.count() > 0:
			questionnaire = questionnaires[0]
			questionids = json.loads(questionnaire.questionids)
			logging.warning('questionids:%s' %questionids)
			for questionid in questionids:
				logging.warning('questionid:%s' %questionid)
				question_dict = dict()
				question_dict['surveyid'] = surveyid
				question_dict['questionid'] = questionid
				question_dict['question'] = Questions.objects.filter(questionid=questionid)[0].question
				rowslist.append(question_dict)
	res['rows'] = rowslist
	res['total'] = len(rowslist)					
	return HttpResponse(json.dumps(res).decode("unicode-escape"))


# 根据问题ID查找题目中各选项被选择情况
def question_selected_info(request):
	res = dict()
	rowslist = list()
	if 'questionid' in request.REQUEST and'surveyid' in request.REQUEST and 'question_index' in request.REQUEST:
		questionid = int(request.REQUEST.get('questionid'))
		surveyid = int(request.REQUEST.get('surveyid'))
		
		question_index = int(request.REQUEST.get('question_index'))
		questions = Questions.objects.filter(questionid=questionid)
		surveys = Surveys.objects.filter(surveyid=surveyid)
		
		if questions.count() > 0 and surveys.count() > 0:
			question = questions[0]
			answers = json.loads(question.answers)
			survey = surveys[0]
			result = json.loads(survey.result)[question_index]
			logging.warning('result:%s' %result)
			#logging.warning(result[0])
			index = 0
			for answer in answers:
				tempdict = dict()
				tempdict['answer'] = answer
				logging.warning('index:%s' %index)
				tempdict['select_count'] = result[index]
				logging.warning('select_count:%s' %result[index])
				index = index + 1
				rowslist.append(tempdict)
	res['rows'] = rowslist
	res['total'] = len(rowslist)	
	return HttpResponse(json.dumps(res).decode("unicode-escape"))


# 根据商场ID和问卷ID，设置指定商场的当前问卷
def set_questionnaire(request):
	res = dict()
	if 'marketid' in request.REQUEST and 'questionnaireid' in request.REQUEST:
		marketid = request.REQUEST.get('marketid')
		questionnaireid = int(request.REQUEST.get('questionnaireid'))
		if Markets.objects.filter(marketid=marketid).count() <= 0:
			res['status'] = 0
			res['msg'] = '商场不存在，请刷新页面后重试。'		
			return HttpResponse(json.dumps(res).decode("unicode-escape"))
		market = Markets.objects.filter(marketid=marketid)[0]
		if Questionnaires.objects.filter(questionnaireid=questionnaireid).count() <= 0:
			res['status'] = 0
			res['msg'] = '问卷不存在，请刷新页面后重试。'	
			return HttpResponse(json.dumps(res).decode("unicode-escape"))		
		questionnaire = Questionnaires.objects.filter(questionnaireid=questionnaireid)[0]	
		surveys = Surveys.objects.filter(marketid=marketid).order_by('-surveyid')
		now = int(time.time())
		if surveys.count() > 0:
			# 找出商场的当前问卷，如果跟此次设置的问卷相同，则返回设置失败；如果不同，则终止当前问卷活动，设置其endtime为当前时间戳。
			survey = surveys[0]
			if survey.questionnaireid.questionnaireid == questionnaireid:
				res['status'] = 0
				res['msg'] = '该问卷已是当前问卷'
				return HttpResponse(json.dumps(res).decode("unicode-escape"))
			else:
				survey.endtime = now
				survey.save()
		# 最后添加此次设置的问卷到数据库。
		# 依次查找该问卷中各题目的答案数
		result = list()
		questionids = json.loads(questionnaire.questionids)
		for questionid in questionids:
			logging.warning('questionid:%s' %questionid)
			questions = Questions.objects.filter(questionid=questionid)
			if questions.count() <= 0:
				res['status'] = 0
				res['msg'] = '数据错误，问卷号【%s】中包含的问题编号【%s】找不到，请联系管理员'  %(questionnaireid, questionid)
				return HttpResponse(json.dumps(res).decode("unicode-escape"))
			else:
				logging.warning('000')
				question = questions[0]
				answers = json.loads(question.answers)
				logging.warning('111')
				result_inner = list()
				for answer in answers:
					result_inner.append(0)
				logging.warning('222')
				result.append(result_inner)
				logging.warning('333')
		logging.warning('result:%s' %json.dumps(result))
		Surveys.objects.create(questionnaireid=questionnaire, marketid = market, result=json.dumps(result), begintime=now)
		logging.warning('444')
		res['status'] = 1	
	else:
		res['status'] = 0
		res['msg'] = '请选择问卷号之后再进行查询'
	return HttpResponse(json.dumps(res).decode("unicode-escape"))


# 导出问卷数据到EXCEL
def save_questionnaire_to_excel(request):
	if 'surveyid' in request.REQUEST:
		surveyid = request.REQUEST.get('surveyid')
		if surveyid == "":
			return HttpResponse('暂无数据')
		surveys = Surveys.objects.filter(surveyid=surveyid)
		if surveys.count() < 1:
			return HttpResponse('暂无数据')
		survey = surveys[0]
		questionnaireid = survey.questionnaireid.questionnaireid
		questionids = json.loads(survey.questionnaireid.questionids)
		marketid = survey.marketid.marketid
		marketname = survey.marketid.marketname
		result = json.loads(survey.result)
		answer_max_count = 0			# 问卷中所有问题中，答案最多的个数，用于定制EXCEL表头标题
		excel_row = 1 	# 当前写到EXCEL的行索引
		question_index = 0
		wb = xlwt.Workbook(encoding='utf-8')
		#title = '%s_%s_%s' %(marketid, questionnaireid, time.strftime('%Y%m%d',time.localtime(time.time())))
		ws = wb.add_sheet('问卷统计' )
		ws.write(0, 0, '商场编号')
		ws.write(0, 1, '商场名称')
		ws.write(0, 2, '问卷编号')
		ws.write(0, 3, '题目编号')
		ws.write(0, 4, '问题类型')
		ws.write(0, 5, '问题')
		for questionid in questionids:
			question_objs = Questions.objects.filter(questionid=questionid)
			question_obj = question_objs[0]
			question = question_obj.question
			questiontype = question_obj.questiontypeid.questiontype
			answers = json.loads(question_obj.answers)
			if len(answers) > answer_max_count:
				answer_max_count = len(answers)
			ws.write(excel_row, 0, marketid)
			ws.write(excel_row, 1, marketname)
			ws.write(excel_row, 2, questionnaireid)
			ws.write(excel_row, 3, questionid)
			ws.write(excel_row, 4, questiontype)
			ws.write(excel_row, 5, question)
			answer_index = 0
			for answer in answers:
				answer_sum = 0
				for i in range(0,len(answers)):
					answer_sum += int( result[question_index][i])
				logging.warning('answer_sum:%s' %answer_sum)
				ws.write(excel_row, 6+(answer_index*3+0), answer)
				ws.write(excel_row, 6+(answer_index*3+1), result[question_index][answer_index])
				if answer_sum == 0:
					ws.write(excel_row, 6+(answer_index*3+2),'0.00%')
				else:
					ws.write(excel_row, 6+(answer_index*3+2), '%.2f%%' %(float(result[question_index][answer_index])/answer_sum*100))
				answer_index = answer_index + 1
			question_index = question_index + 1
			excel_row = excel_row + 1
		# 根据最大答案数，补全EXCEL标题栏：
		for i in range(0,answer_max_count):
			ws.write(0, 6 + (i*3+0), '选项%s' %(i+1))
			ws.write(0, 6 + (i*3+1), '选项%s选中数量' %(i+1))
			ws.write(0, 6 + (i*3+2), '占总数量的百分比')
		response = HttpResponse(content_type='application/vnd.ms-excel')
		response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, questionnaireid, time.strftime('%Y%m%d',time.localtime(time.time())))
		wb.save(response)
		return response
	else:
		return HttpResponse('暂无数据')



# 获取配件价格列表的模板页面，返回默认商场编号
def units(request):
	res = dict()
	marketid = str()
	if request.GET.get('marketid'):
		marketid = request.GET.get('marketid')
	else:
		marketid = request.session["marketid"]
	res["marketid"] = marketid
	return render_to_response('units.html', res)
	

# 获取配件价格列表，若请求中带有excel字段，则返回excel文件下载。基本类同的编辑，就不分两个方法了。
def units_data(request):
	res = dict()
	rowslist = list()
	page = 1
	rows = 100000000
	if 'page' in request.REQUEST and 'rows' in request.REQUEST:
		page = int(request.REQUEST.get('page'))
		rows = int(request.REQUEST.get('rows'))
	min = 0
	max = 0
	min = (page-1) * rows
	max = page * rows
	if 'unitid' in request.REQUEST and 'marketid' in request.REQUEST:
		unitid = request.REQUEST.get('unitid')
		marketid = request.REQUEST.get('marketid')
		logging.warning('unitid:%s  marketid:%s' %(unitid,marketid))
		objs = Units.objects.filter(unitid__contains=unitid, marketid=marketid)
	else:
		marketid = request.session["marketid"]
		objs = Units.objects.filter(marketid=marketid)
	total = objs.count()
	if 'excel' in request.REQUEST:
		# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
		wb = xlwt.Workbook(encoding='utf-8')
		ws = wb.add_sheet('配件价格')
		ws.write(0, 0, '配件编号')
		ws.write(0, 1, '配件名称')
		ws.write(0, 2, '配件价格')
		ws.write(0, 3, '商场')
		excel_row = 1
		for obj in objs:
			ws.write(excel_row, 0, obj.unitid)
			ws.write(excel_row, 1, obj.unitname)
			ws.write(excel_row, 2, obj.price)
			ws.write(excel_row, 3, obj.marketid)
			excel_row = excel_row + 1
		response = HttpResponse(content_type='application/vnd.ms-excel')
		response['Content-Disposition'] = 'attachment; filename=data.xls'
		wb.save(response)
		return response
	for obj in objs[min:max]:
		unit_dict = dict()
		unit_dict["marketid"] = obj.marketid
		unit_dict["unitid"] = obj.unitid
		unit_dict["unitname"] = obj.unitname
		unit_dict["price"] = obj.price
		rowslist.append(unit_dict)
	res['rows'] = rowslist
	res['total'] = total
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 保存配件价格数据
def unit_save(request):
	res = dict()
	res['status'] = 0
	if request.POST.get('unitid') and request.POST.get('price') and request.POST.get('marketid'):
		marketid = request.POST.get('marketid')
		unitid = request.POST.get('unitid')
		price = request.POST.get('price')
		try:
			#Units.objects.filter(unitid=unitid).update(price=price)
			markets = Markets.objects.filter(marketid=marketid)
			market = markets[0]
			#logging.warning(market.marketname.decode("unicode-escape"))
			content = u'%s %s 管理员修改了 %s 的价格' %(marketid, market.marketname.decode("utf-8"), unitid)
			#logging.warning(content)
			send_mail(u'编辑价格提醒', content)
			res['status'] = 1
		except Exception,e:
			logging.warning("Exception:%s" %e)
			res['msg'] = '保存错误，请联系管理员。'
	else:
		res['msg'] = '提交请求参数错误，请检查。'
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res ->  %s " %res)
	return HttpResponse(res)


# 获取活动数据
def get_activity(request):
	res = dict()
	res['status'] = 0
	activity = dict()
	marketid = ""
	if request.GET.get('marketid'):
		# 如果请求中带有marketid，则返回指定商场ID
		marketid = request.GET.get('marketid')
		if marketid=='all':
			marketid = request.session['marketid']
	else:
		# 若请求中没有带marketid，则返回本帐号所在商场ID
		marketid = request.session['marketid']		
	activity['marketid'] = marketid
	activity['marketname'] = Markets.objects.get(marketid=marketid).marketname
	# 查找指定商场最新的活动，不管是不是当前正在进行中，只要是最新活动即显示出来
	objs = Activities.objects.filter(marketid=marketid).order_by('-activityid')
	if objs.count() > 0:
		obj = objs[0]	
		activity['content'] = obj.content
		activity['image1'] = '%s?random=%s' %(json.loads(obj.imagenames)[0], random.random())
		#activity['image2'] = '%s?random=%s' %(json.loads(obj.imagenames)[1], random.random())
		activity['begintime'] = str(obj.begintime)
		activity['endtime'] = str(obj.endtime)
		res = json.dumps(activity).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return render_to_response('activity.html',  activity)
	else:
		activity['content'] = ""
		activity['image1'] = ""
		#activity['image2'] =""
		activity['begintime'] = ""
		activity['endtime'] = ""
		res = json.dumps(activity).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return render_to_response('activity.html',  activity)


# 更新活动
def update_activity(request):
	res = dict()
	res['status'] = 0	
	if request.POST.get('marketid') and request.POST.get('content') and request.POST.get('begintime') and request.POST.get('endtime'):
		marketid = request.POST.get('marketid')
		content = request.POST.get('content')
		image1 = request.POST.get('image1')
		default_image = request.POST.get('default_image')
		
		new_image_name = default_image[0:default_image.find('?')]
		logging.warning('new_image_name:%s' %new_image_name)
		#image2 = request.POST.get('image2')
		begintime = request.POST.get('begintime')
		endtime = request.POST.get('endtime')
		# 校验上传的组合方案配置文件
		if 'image1' in request.FILES:
			image1 = request.FILES['image1']		
			image1name = image1.name
			if image1name.split('.')[-1]  != 'png' :
				res["msg"] = "上传的活动图片后缀名不正确，需要上传 *.png 文件。"	
				res = json.dumps(res).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			"""
			# 活动图片由原先的两张改成一张
			# 校验上传的组合方案内部结构图
			image2 = request.FILES['image2']
			image2name = image2.name
			if image2name.split('.')[-1]  != 'png' :
				res["msg"] = "上传的活动图片后缀名不正确，需要上传 *.png 文件。"	
				res = json.dumps(res).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			"""
			# 上传两张活动图片，并组织好文件名数据，以记录到数据库
			upload_dir = "/htdocs/ikea/media/activity/"
			now = int(time.time())
			new_image_name = "%s_a.png" %now
			#image2_newname = "%s_b.png" %now
			handle_uploaded_file(image1, upload_dir, new_image_name)
		#handle_uploaded_file(image2, upload_dir, image2_newname)				
		imagenames_list = list()
		imagenames_list.append(new_image_name)
		#imagenames_list.append(image2_newname)
		imagenames = json.dumps(imagenames_list).decode("unicode-escape")
		
		# 根据是全国批量设置还是指定商场设置，来写入数据到数据库
		if marketid == "all":
			# 如果是全国批量更新：
			logging.warning('all')
			markets = Markets.objects.all()
			for market in markets:
				Activities.objects.create(marketid=market , content=content, imagenames=imagenames, begintime=begintime, endtime=endtime)			
			res['status'] = 1
			res = json.dumps(res).decode("unicode-escape")
			logging.warning("res ->  %s " %res)
			return HttpResponse(res)
		else:
			# 更新指定商场活动：
			Activities.objects.create(marketid=Markets.objects.get(marketid=marketid) , content=content, imagenames=imagenames, begintime=begintime, endtime=endtime)			
			res['status'] = 1
			res = json.dumps(res).decode("unicode-escape")
			logging.warning("res ->  %s " %res)
			return HttpResponse(res)
	else:
		logging.warning('11111111111')
		
		
# 显示推荐组合一字柜方案页面模板，传回默认商场
def default_design_a(request):
	res = dict()
	marketid = str()
	if request.GET.get('marketid'):
		marketid = request.GET.get('marketid')
	else:
		marketid = request.session["marketid"]
	res["marketid"] = marketid
	return render_to_response('default_design_a.html', res)
	
		
# 显示推荐组合一字柜方案页面中的 datagrid 数据源
def default_design_a_data(request):
	res = dict()
	rowslist = list()
	if request.POST.get('page') and request.POST.get('rows'):
		page = int(request.POST.get('page'))
		rows = int(request.POST.get('rows'))
		min = 0
		max = 0
		min = (page-1) * rows
		max = page * rows
		total = 0
		marketid = ""
		if request.POST.get('marketid'):
			marketid = request.POST.get('marketid')
		else:
			marketid = request.session["marketid"]
		logging.warning("marketid:%s" %marketid)
		objs = Default_design_a.objects.filter(marketid=marketid, show=1)[min:max]
		total = Default_design_a.objects.filter(marketid=marketid, show=1).count()
		for obj in objs:
			obj_dict = dict()
			obj_dict["designid"] = obj.designid
			obj_dict["designname1"] = obj.designname1
			obj_dict["designname2"] = obj.designname2
			obj_dict["marketid"] = obj.marketid
			#obj_dict["cover"] = obj.cover
			rowslist.append(obj_dict)
		res['rows'] = rowslist
		res['total'] = total
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)


# 显示推荐组合一字柜方案页面中的 datagrid 数据源，通过商场查找
def default_design_a_data_by_marketid(request):
	res = dict()
	rowslist = list()
	if request.POST.get('page') and request.POST.get('rows'):
		page = int(request.POST.get('page'))
		rows = int(request.POST.get('rows'))
		min = 0
		max = 0
		min = (page-1) * rows
		max = page * rows
		total = 0
		marketid = ""
		if request.POST.get('marketid'):
			marketid = request.POST.get('marketid')
		else:
			marketid = request.session["marketid"]
		logging.warning("marketid:%s" %marketid)
		objs = Default_design_a.objects.filter(marketid=marketid, show=1)[min:max]
		total = Default_design_a.objects.filter(marketid=marketid, show=1).count()
		for obj in objs:
			obj_dict = dict()
			obj_dict["designid"] = obj.designid
			obj_dict["designname"] = obj.designname1
			obj_dict["marketid"] = obj.marketid
			#obj_dict["cover"] = obj.cover
			rowslist.append(obj_dict)
		res['rows'] = rowslist
		res['total'] = total
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)
		
	
# 获取推荐组合管理一字柜中某一项的编辑信息
def edit_default_design_a(request):
	res = dict()
	if request.GET.get('designid'):
		designid = request.GET.get('designid')		
		obj = Default_design_a.objects.get(designid=designid)
		res["designid"] = designid
		res["designname1"] = obj.designname1 if obj.designname1 else "" 
		res["designname2"] = obj.designname2 if obj.designname2 else "" 
		designdesc = json.loads(obj.designdesc)	
		logging.warning(designdesc[0])	
		res["designdesc1"] = designdesc[0] 
		res["designdesc2"] = designdesc[1] 
		res["designdesc3"] = designdesc[2]
		res["designdesc4"] = designdesc[3]
		res["designdesc5"] = designdesc[4]
		res["designdesc6"] = designdesc[5]
		#下面用了一技巧，在图片URL后面带上一个随机数，这是因为当图片更新但图片名没更新时，浏览器很可能因为缓存机制而不刷新图片。
		res["cover"] = "/media/default_design_a/%s_cover.png?random=%s" %(obj.designid,random.random())
	#res = json.dumps(res).decode("unicode-escape")
	#logging.warning("res ->  %s " %res)
	return render_to_response('edit_default_design_a.html', res)


# 保存推荐方案一字柜编辑信息
def save_default_design_a(request):
	res = dict()
	#if request.POST.get('designid') and request.POST.get('designname1') and request.POST.get('designname2') and request.POST.get('designdesc1') and request.POST.get('designdesc2') and request.POST.get('designdesc3') and request.POST.get('designdesc4') and request.POST.get('designdesc5') and request.POST.get('designdesc6'):
	if True:
		designid = request.POST.get('designid')
		designname1 = request.POST.get('designname1')
		designname2 = request.POST.get('designname2')
		designdesc1 = request.POST.get('designdesc1')
		designdesc2 = request.POST.get('designdesc2')
		designdesc3 = request.POST.get('designdesc3')
		designdesc4 = request.POST.get('designdesc4')
		designdesc5 = request.POST.get('designdesc5')
		designdesc6 = request.POST.get('designdesc6')
		# 校验上传的封面图片，注意要使用下面的方式，而不是 if request.FILES['cover']: 
		if 'cover' in request.FILES:
			cover = request.FILES['cover']
			covername = cover.name
			if covername.split('.')[-1]  != 'png' :
				res["status"] = 0
				res["msg"] = "上传的图片后缀名不正确，需要上传 *.png 文件。"	
				res = json.dumps(res).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			else:
				upload_dir = "media/default_design_a/"
				cover_newname = "%s_cover.png" %designid
				handle_uploaded_file(cover, upload_dir, cover_newname)		
		obj = Default_design_a.objects.get(designid=designid)
		designdesc = list()
		designdesc.append(designdesc1)
		designdesc.append(designdesc2)
		designdesc.append(designdesc3)
		designdesc.append(designdesc4)
		designdesc.append(designdesc5)
		designdesc.append(designdesc6)
		Default_design_a.objects.filter(designid=designid).update(designname1=designname1, designname2=designname2, designdesc=json.dumps(designdesc).decode("unicode-escape"), updatetime=int(time.time()))
		res["status"] = 1
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res: %s" %res)
		#marketid = Default_design_a.objects.filter(designid=designid)[0].marketid
		#return render_to_response('default_design_a.html', {"marketid":marketid})
		return HttpResponse(res)
	else:
		res["status"] = 0
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res: %s" %res)
		return HttpResponse(res)		


# 删除推荐组合一字柜（设为非活跃）
def delete_default_design_a(request):
	res = dict()
	if request.REQUEST.get('designid'):
		designid = int(request.REQUEST.get('designid'))
		objs = Default_design_a.objects.filter(designid=designid)
		if objs.count()>0:
			Default_design_a.objects.filter(designid=designid).update(show=0)
	res['status'] = 1
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)	
	
	
	# 显示推荐组合转角柜方案页面模板，传回默认商场
def default_design_b(request):
	res = dict()
	marketid = str()
	if request.GET.get('marketid'):
		marketid = request.GET.get('marketid')
	else:
		marketid = request.session["marketid"]
	res["marketid"] = marketid
	return render_to_response('default_design_b.html', res)
	
		
# 显示推荐组合转角柜方案页面中的 datagrid 数据源
def default_design_b_data(request):
	res = dict()
	rowslist = list()
	if request.POST.get('page') and request.POST.get('rows'):
		page = int(request.POST.get('page'))
		rows = int(request.POST.get('rows'))
		min = 0
		max = 0
		min = (page-1) * rows
		max = page * rows
		total = 0
		marketid = ""
		if request.POST.get('marketid'):
			marketid = request.POST.get('marketid')
		else:
			marketid = request.session["marketid"]
		logging.warning("marketid:%s" %marketid)
		objs = Default_design_b.objects.filter(marketid=marketid, show=1)[min:max]
		total = Default_design_b.objects.filter(marketid=marketid, show=1).count()
		for obj in objs:
			obj_dict = dict()
			obj_dict["designid"] = obj.designid
			obj_dict["designname1"] = obj.designname1
			obj_dict["designname2"] = obj.designname2
			obj_dict["marketid"] = obj.marketid
			#obj_dict["cover"] = obj.cover
			rowslist.append(obj_dict)
		res['rows'] = rowslist
		res['total'] = total
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)


# 显示推荐组合转角柜方案页面中的 datagrid 数据源，通过商场查找
def default_design_b_data_by_marketid(request):
	res = dict()
	rowslist = list()
	if request.POST.get('page') and request.POST.get('rows'):
		page = int(request.POST.get('page'))
		rows = int(request.POST.get('rows'))
		min = 0
		max = 0
		min = (page-1) * rows
		max = page * rows
		total = 0
		marketid = ""
		if request.POST.get('marketid'):
			marketid = request.POST.get('marketid')
		else:
			marketid = request.session["marketid"]
		logging.warning("marketid:%s" %marketid)
		objs = Default_design_b.objects.filter(marketid=marketid, show=1)[min:max]
		total = Default_design_b.objects.filter(marketid=marketid, show=1).count()
		for obj in objs:
			obj_dict = dict()
			obj_dict["designid"] = obj.designid
			obj_dict["designname"] = obj.designname1
			obj_dict["marketid"] = obj.marketid
			#obj_dict["cover"] = obj.cover
			rowslist.append(obj_dict)
		res['rows'] = rowslist
		res['total'] = total
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)
		
	
# 获取推荐组合管理转角柜中某一项的编辑信息
def edit_default_design_b(request):
	res = dict()
	if request.GET.get('designid'):
		designid = request.GET.get('designid')		
		obj = Default_design_b.objects.get(designid=designid)
		res["designid"] = designid
		res["designname1"] = obj.designname1 if obj.designname1 else "" 
		res["designname2"] = obj.designname2 if obj.designname2 else "" 
		designdesc = json.loads(obj.designdesc)	
		logging.warning(designdesc[0])	
		res["designdesc1"] = designdesc[0] 
		res["designdesc2"] = designdesc[1] 
		res["designdesc3"] = designdesc[2]
		res["designdesc4"] = designdesc[3]
		res["designdesc5"] = designdesc[4]
		res["designdesc6"] = designdesc[5]
		#下面用了一技巧，在图片URL后面带上一个随机数，这是因为当图片更新但图片名没更新时，浏览器很可能因为缓存机制而不刷新图片。
		res["cover"] = "/media/default_design_b/%s_cover.png?random=%s" %(obj.designid,random.random())
	#res = json.dumps(res).decode("unicode-escape")
	#logging.warning("res ->  %s " %res)
	return render_to_response('edit_default_design_b.html', res)


# 保存推荐方案转角柜编辑信息
def save_default_design_b(request):
	res = dict()
	#if request.POST.get('designid') and request.POST.get('designname1') and request.POST.get('designname2') and request.POST.get('designdesc1') and request.POST.get('designdesc2') and request.POST.get('designdesc3') and request.POST.get('designdesc4') and request.POST.get('designdesc5') and request.POST.get('designdesc6'):
	if True:
		designid = request.POST.get('designid')
		designname1 = request.POST.get('designname1')
		designname2 = request.POST.get('designname2')
		designdesc1 = request.POST.get('designdesc1')
		designdesc2 = request.POST.get('designdesc2')
		designdesc3 = request.POST.get('designdesc3')
		designdesc4 = request.POST.get('designdesc4')
		designdesc5 = request.POST.get('designdesc5')
		designdesc6 = request.POST.get('designdesc6')
		# 校验上传的封面图片，注意要使用下面的方式，而不是 if request.FILES['cover']: 
		if 'cover' in request.FILES:
			cover = request.FILES['cover']
			covername = cover.name
			if covername.split('.')[-1]  != 'png' :
				res["status"] = 0
				res["msg"] = "上传的图片后缀名不正确，需要上传 *.png 文件。"	
				res = json.dumps(res).decode("unicode-escape")
				logging.warning("res: %s" %res)
				return HttpResponse(res)
			else:
				upload_dir = "/htdocs/ikea/media/default_design_b/"
				cover_newname = "%s_cover.png" %designid
				handle_uploaded_file(cover, upload_dir, cover_newname)		
		obj = Default_design_b.objects.get(designid=designid)
		designdesc = list()
		designdesc.append(designdesc1)
		designdesc.append(designdesc2)
		designdesc.append(designdesc3)
		designdesc.append(designdesc4)
		designdesc.append(designdesc5)
		designdesc.append(designdesc6)
		Default_design_b.objects.filter(designid=designid).update(designname1=designname1, designname2=designname2, designdesc=json.dumps(designdesc).decode("unicode-escape"), updatetime=int(time.time()))
		res["status"] = 1
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res: %s" %res)
		#marketid = Default_design_b.objects.filter(designid=designid)[0].marketid
		#return render_to_response('default_design_b.html', {"marketid":marketid})
		return HttpResponse(res)
	else:
		res["status"] = 0
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res: %s" %res)
		return HttpResponse(res)		


# 删除推荐组合转角柜（设为非活跃）
def delete_default_design_b(request):
	res = dict()
	if request.REQUEST.get('designid'):
		designid = int(request.REQUEST.get('designid'))
		objs = Default_design_b.objects.filter(designid=designid)
		if objs.count()>0:
			Default_design_b.objects.filter(designid=designid).update(show=0)
	res['status'] = 1
	res = json.dumps(res).decode("unicode-escape")
	logging.warning("res: %s" %res)
	return HttpResponse(res)	
	
	
# 显示统计页面
def statistics(request):
	res = dict()
	marketid = str()
	if request.GET.get('marketid'):
		marketid = request.GET.get('marketid')
	else:
		marketid = request.session["marketid"]
	res["marketid"] = marketid
	return render_to_response('statistics.html', res)


# 显示价格百分比统计页面
def statistics_price(request):
    res = dict()
    marketid = str()
    if request.GET.get('marketid'):
        marketid = request.GET.get('marketid')
    else:
        marketid = request.session["marketid"]
    res["marketid"] = marketid
    return render_to_response('statistics_price.html', res)

def design_statistics_price_data(request):
	def GetPercent(num, total): 
		if total == 0:
			return '0.00%'
		else:
			ret = float(num)/float(total)*100
			#logging.warning('ret:%s' %ret)
			return '%s%%' %round(ret,2)
	def get_percent_list(lt):
		sum = len(lt)
		new_list = list()
		for item in lt:
			count = 0
			for item2 in lt:
				if item2 == item:
					count = count + 1
			new_list.append(GetPercent(count, sum))
		return new_list
	res = dict();
	if request.REQUEST.get('marketid') and request.REQUEST.get('time1') and request.REQUEST.get('time2') and request.REQUEST.get('designtype') \
	and 'design_name_like' in request.REQUEST:		
		marketid = request.REQUEST.get('marketid')
		time1 = request.REQUEST.get('time1')
		time1 = int(time.mktime(time.strptime(time1,'%Y-%m-%d')))
		time2 = request.REQUEST.get('time2')
		# 将最大天数加上一天，以方便后面获取包含当天的数据
		time2 = int(time.mktime(time.strptime(time2,'%Y-%m-%d'))) + 60*60*24
		#logging.warning('time1:%s\ttime2:%s' %(time1,time2))
		designtype = request.REQUEST.get('designtype')
		logging.warning('designtype:%s' %designtype)
		design_name_like = request.REQUEST.get('design_name_like')
		page = 1
		rows = 100000000
		if 'page' in request.REQUEST and 'rows' in request.REQUEST:
			page = int(request.REQUEST.get('page'))
			rows = int(request.REQUEST.get('rows'))		
		min = (page-1) * rows
		max = page * rows
		logging.warning("min:%s\tmax:%s" %(min,max))
		if designtype== "default_design_a":
			sql = "select markets.marketid as marketid,marketname,sum(case when price BETWEEN 0 and 10000 then 1 else 0 end) as tp1,sum(case when price BETWEEN 10001 and 30000 then 1 else 0 end) as tp2,sum(case when price BETWEEN 30001 and 50000 then 1 else 0 end) as tp3,sum(case when price BETWEEN 50001 and 70000 then 1 else 0 end) as tp4,sum(case when price >70001 then 1 else 0 end) as tp5,count(*) as tpall from default_design_a inner join markets on markets.marketid=default_design_a.marketid inner join default_design_statistics on default_design_a.designid=default_design_statistics.designid where action='buy' and markets.marketid='%s' AND createtime BETWEEN %s and %s group by markets.marketid,marketname" \
				%(marketid,time1,time2)
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(0)
					row_list.append(row[3])
					row_list.append(0)
					row_list.append(row[4])
					row_list.append(0)
					row_list.append(row[5])
					row_list.append(0)
					row_list.append(row[6])
					row_list.append(0)
					row_list.append(row[7])
					data_list.append(row_list)
				for i in range(0,len(data_list)):
					data_list[i][3] = GetPercent(int(data_list[i][2]), int(data_list[i][12]))
					data_list[i][5] = GetPercent(int(data_list[i][4]), int(data_list[i][12]))
					data_list[i][7] = GetPercent(int(data_list[i][6]), int(data_list[i][12]))
					data_list[i][9] = GetPercent(int(data_list[i][8]), int(data_list[i][12]))
					data_list[i][11] = GetPercent(int(data_list[i][10]), int(data_list[i][12]))
			logging.warning("data_list.size:%s" %len(data_list))
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('价格百分比数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '0-1000数量')
				ws.write(0, 3, '0-1000百分比')
				ws.write(0, 4, '1001-3000数量')
				ws.write(0, 5, '1001-3000百分比')
				ws.write(0, 6, '3001-5000数量')
				ws.write(0, 7, '3001-5000百分比')
				ws.write(0, 8, '5001-7000数量')
				ws.write(0, 9, '5001-7000百分比')
				ws.write(0, 10, '大于7001数量')
				ws.write(0, 11, '大于7001百分比')
				ws.write(0, 12, '总数量')				
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,13):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('推荐组合一字柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response			
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'default_design_a'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['tp1'] = int(row[2])
				itemdict['ptp1'] = row[3]
				itemdict['tp2'] = int(row[4])
				itemdict['ptp2'] = row[5]
				itemdict['tp3'] = int(row[6])
				itemdict['ptp3'] = row[7]
				itemdict['tp4'] = int(row[8])
				itemdict['ptp4'] = row[9]
				itemdict['tp5'] = int(row[10])
				itemdict['ptp5'] = row[11]
				itemdict['tpall'] = int(row[12])
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data)
		elif designtype== "default_design_b":
			sql = "select markets.marketid as marketid,marketname,sum(case when price BETWEEN 0 and 10000 then 1 else 0 end) as tp1,sum(case when price BETWEEN 10001 and 30000 then 1 else 0 end) as tp2,sum(case when price BETWEEN 30001 and 50000 then 1 else 0 end) as tp3,sum(case when price BETWEEN 50001 and 70000 then 1 else 0 end) as tp4,sum(case when price >70001 then 1 else 0 end) as tp5,count(*) as tpall from default_design_b inner join markets on markets.marketid=default_design_b.marketid inner join default_design_statistics on default_design_b.designid=default_design_statistics.designid where action='buy' and markets.marketid='%s' AND createtime BETWEEN %s and %s group by markets.marketid,marketname" \
				%(marketid,time1,time2)
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(0)
					row_list.append(row[3])
					row_list.append(0)
					row_list.append(row[4])
					row_list.append(0)
					row_list.append(row[5])
					row_list.append(0)
					row_list.append(row[6])
					row_list.append(0)
					row_list.append(row[7])
					data_list.append(row_list)
				for i in range(0,len(data_list)):
					data_list[i][3] = GetPercent(int(data_list[i][2]), int(data_list[i][12]))
					data_list[i][5] = GetPercent(int(data_list[i][4]), int(data_list[i][12]))
					data_list[i][7] = GetPercent(int(data_list[i][6]), int(data_list[i][12]))
					data_list[i][9] = GetPercent(int(data_list[i][8]), int(data_list[i][12]))
					data_list[i][11] = GetPercent(int(data_list[i][10]), int(data_list[i][12]))
			logging.warning("data_list.size:%s" %len(data_list))
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('价格百分比数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '0-1000数量')
				ws.write(0, 3, '0-1000百分比')
				ws.write(0, 4, '1001-3000数量')
				ws.write(0, 5, '1001-3000百分比')
				ws.write(0, 6, '3001-5000数量')
				ws.write(0, 7, '3001-5000百分比')
				ws.write(0, 8, '5001-7000数量')
				ws.write(0, 9, '5001-7000百分比')
				ws.write(0, 10, '大于7001数量')
				ws.write(0, 11, '大于7001百分比')
				ws.write(0, 12, '总数量')				
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,13):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('推荐组合转角柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response			
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'default_design_b'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['tp1'] = int(row[2])
				itemdict['ptp1'] = row[3]
				itemdict['tp2'] = int(row[4])
				itemdict['ptp2'] = row[5]
				itemdict['tp3'] = int(row[6])
				itemdict['ptp3'] = row[7]
				itemdict['tp4'] = int(row[8])
				itemdict['ptp4'] = row[9]
				itemdict['tp5'] = int(row[10])
				itemdict['ptp5'] = row[11]
				itemdict['tpall'] = int(row[12])
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data)
		elif designtype== "reference_design_a":
			sql = "select markets.marketid as marketid,marketname,sum(case when price BETWEEN 0 and 10000 then 1 else 0 end) as tp1,sum(case when price BETWEEN 10001 and 30000 then 1 else 0 end) as tp2,sum(case when price BETWEEN 30001 and 50000 then 1 else 0 end) as tp3,sum(case when price BETWEEN 50001 and 70000 then 1 else 0 end) as tp4,sum(case when price >70001 then 1 else 0 end) as tp5,count(*) as tpall from reference_design_a inner join markets on markets.marketid=reference_design_a.marketid where markets.marketid='%s' AND createtime BETWEEN %s and %s group by markets.marketid,marketname" \
				%(marketid,time1,time2)
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(0)
					row_list.append(row[3])
					row_list.append(0)
					row_list.append(row[4])
					row_list.append(0)
					row_list.append(row[5])
					row_list.append(0)
					row_list.append(row[6])
					row_list.append(0)
					row_list.append(row[7])
					data_list.append(row_list)
				for i in range(0,len(data_list)):
					data_list[i][3] = GetPercent(int(data_list[i][2]), int(data_list[i][12]))
					data_list[i][5] = GetPercent(int(data_list[i][4]), int(data_list[i][12]))
					data_list[i][7] = GetPercent(int(data_list[i][6]), int(data_list[i][12]))
					data_list[i][9] = GetPercent(int(data_list[i][8]), int(data_list[i][12]))
					data_list[i][11] = GetPercent(int(data_list[i][10]), int(data_list[i][12]))
			logging.warning("data_list.size:%s" %len(data_list))
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('价格百分比数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '0-1000数量')
				ws.write(0, 3, '0-1000百分比')
				ws.write(0, 4, '1001-3000数量')
				ws.write(0, 5, '1001-3000百分比')
				ws.write(0, 6, '3001-5000数量')
				ws.write(0, 7, '3001-5000百分比')
				ws.write(0, 8, '5001-7000数量')
				ws.write(0, 9, '5001-7000百分比')
				ws.write(0, 10, '大于7001数量')
				ws.write(0, 11, '大于7001百分比')
				ws.write(0, 12, '总数量')				
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,13):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('组合设计一字柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response			
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'reference_design_a'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['tp1'] = int(row[2])
				itemdict['ptp1'] = row[3]
				itemdict['tp2'] = int(row[4])
				itemdict['ptp2'] = row[5]
				itemdict['tp3'] = int(row[6])
				itemdict['ptp3'] = row[7]
				itemdict['tp4'] = int(row[8])
				itemdict['ptp4'] = row[9]
				itemdict['tp5'] = int(row[10])
				itemdict['ptp5'] = row[11]
				itemdict['tpall'] = int(row[12])
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data)
		elif designtype== "reference_design_b":
			sql = "select markets.marketid as marketid,marketname,sum(case when price BETWEEN 0 and 10000 then 1 else 0 end) as tp1,sum(case when price BETWEEN 10001 and 30000 then 1 else 0 end) as tp2,sum(case when price BETWEEN 30001 and 50000 then 1 else 0 end) as tp3,sum(case when price BETWEEN 50001 and 70000 then 1 else 0 end) as tp4,sum(case when price >70001 then 1 else 0 end) as tp5,count(*) as tpall from reference_design_b inner join markets on markets.marketid=reference_design_b.marketid where markets.marketid='%s' AND createtime BETWEEN %s and %s group by markets.marketid,marketname" \
				%(marketid,time1,time2)
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(0)
					row_list.append(row[3])
					row_list.append(0)
					row_list.append(row[4])
					row_list.append(0)
					row_list.append(row[5])
					row_list.append(0)
					row_list.append(row[6])
					row_list.append(0)
					row_list.append(row[7])
					data_list.append(row_list)
				for i in range(0,len(data_list)):
					data_list[i][3] = GetPercent(int(data_list[i][2]), int(data_list[i][12]))
					data_list[i][5] = GetPercent(int(data_list[i][4]), int(data_list[i][12]))
					data_list[i][7] = GetPercent(int(data_list[i][6]), int(data_list[i][12]))
					data_list[i][9] = GetPercent(int(data_list[i][8]), int(data_list[i][12]))
					data_list[i][11] = GetPercent(int(data_list[i][10]), int(data_list[i][12]))
			logging.warning("data_list.size:%s" %len(data_list))
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('价格百分比数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '0-1000数量')
				ws.write(0, 3, '0-1000百分比')
				ws.write(0, 4, '1001-3000数量')
				ws.write(0, 5, '1001-3000百分比')
				ws.write(0, 6, '3001-5000数量')
				ws.write(0, 7, '3001-5000百分比')
				ws.write(0, 8, '5001-7000数量')
				ws.write(0, 9, '5001-7000百分比')
				ws.write(0, 10, '大于7001数量')
				ws.write(0, 11, '大于7001百分比')
				ws.write(0, 12, '总数量')				
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,13):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('推荐组合转角柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response			
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'reference_design_b'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['tp1'] = int(row[2])
				itemdict['ptp1'] = row[3]
				itemdict['tp2'] = int(row[4])
				itemdict['ptp2'] = row[5]
				itemdict['tp3'] = int(row[6])
				itemdict['ptp3'] = row[7]
				itemdict['tp4'] = int(row[8])
				itemdict['ptp4'] = row[9]
				itemdict['tp5'] = int(row[10])
				itemdict['ptp5'] = row[11]
				itemdict['tpall'] = int(row[12])
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data)
		elif designtype== "independent_design_a":
			sql = "select markets.marketid as marketid,marketname,sum(case when price BETWEEN 0 and 10000 then 1 else 0 end) as tp1,sum(case when price BETWEEN 10001 and 30000 then 1 else 0 end) as tp2,sum(case when price BETWEEN 30001 and 50000 then 1 else 0 end) as tp3,sum(case when price BETWEEN 50001 and 70000 then 1 else 0 end) as tp4,sum(case when price >70001 then 1 else 0 end) as tp5,count(*) as tpall from independent_design_a inner join markets on markets.marketid=independent_design_a.marketid where markets.marketid='%s' AND createtime BETWEEN %s and %s group by markets.marketid,marketname" \
				%(marketid,time1,time2)
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(0)
					row_list.append(row[3])
					row_list.append(0)
					row_list.append(row[4])
					row_list.append(0)
					row_list.append(row[5])
					row_list.append(0)
					row_list.append(row[6])
					row_list.append(0)
					row_list.append(row[7])
					data_list.append(row_list)
				for i in range(0,len(data_list)):
					data_list[i][3] = GetPercent(int(data_list[i][2]), int(data_list[i][12]))
					data_list[i][5] = GetPercent(int(data_list[i][4]), int(data_list[i][12]))
					data_list[i][7] = GetPercent(int(data_list[i][6]), int(data_list[i][12]))
					data_list[i][9] = GetPercent(int(data_list[i][8]), int(data_list[i][12]))
					data_list[i][11] = GetPercent(int(data_list[i][10]), int(data_list[i][12]))
			logging.warning("data_list.size:%s" %len(data_list))
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('价格百分比数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '0-1000数量')
				ws.write(0, 3, '0-1000百分比')
				ws.write(0, 4, '1001-3000数量')
				ws.write(0, 5, '1001-3000百分比')
				ws.write(0, 6, '3001-5000数量')
				ws.write(0, 7, '3001-5000百分比')
				ws.write(0, 8, '5001-7000数量')
				ws.write(0, 9, '5001-7000百分比')
				ws.write(0, 10, '大于7001数量')
				ws.write(0, 11, '大于7001百分比')
				ws.write(0, 12, '总数量')				
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,13):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('推荐组合转角柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response			
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'independent_design_a'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['tp1'] = int(row[2])
				itemdict['ptp1'] = row[3]
				itemdict['tp2'] = int(row[4])
				itemdict['ptp2'] = row[5]
				itemdict['tp3'] = int(row[6])
				itemdict['ptp3'] = row[7]
				itemdict['tp4'] = int(row[8])
				itemdict['ptp4'] = row[9]
				itemdict['tp5'] = int(row[10])
				itemdict['ptp5'] = row[11]
				itemdict['tpall'] = int(row[12])
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data)
		elif designtype== "independent_design_b":
			sql = "select markets.marketid as marketid,marketname,sum(case when price BETWEEN 0 and 10000 then 1 else 0 end) as tp1,sum(case when price BETWEEN 10001 and 30000 then 1 else 0 end) as tp2,sum(case when price BETWEEN 30001 and 50000 then 1 else 0 end) as tp3,sum(case when price BETWEEN 50001 and 70000 then 1 else 0 end) as tp4,sum(case when price >70001 then 1 else 0 end) as tp5,count(*) as tpall from independent_design_b inner join markets on markets.marketid=independent_design_b.marketid where markets.marketid='%s' AND createtime BETWEEN %s and %s group by markets.marketid,marketname" \
				%(marketid,time1,time2)
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(0)
					row_list.append(row[3])
					row_list.append(0)
					row_list.append(row[4])
					row_list.append(0)
					row_list.append(row[5])
					row_list.append(0)
					row_list.append(row[6])
					row_list.append(0)
					row_list.append(row[7])
					data_list.append(row_list)
				for i in range(0,len(data_list)):
					data_list[i][3] = GetPercent(int(data_list[i][2]), int(data_list[i][12]))
					data_list[i][5] = GetPercent(int(data_list[i][4]), int(data_list[i][12]))
					data_list[i][7] = GetPercent(int(data_list[i][6]), int(data_list[i][12]))
					data_list[i][9] = GetPercent(int(data_list[i][8]), int(data_list[i][12]))
					data_list[i][11] = GetPercent(int(data_list[i][10]), int(data_list[i][12]))
			logging.warning("data_list.size:%s" %len(data_list))
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('价格百分比数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '0-1000数量')
				ws.write(0, 3, '0-1000百分比')
				ws.write(0, 4, '1001-3000数量')
				ws.write(0, 5, '1001-3000百分比')
				ws.write(0, 6, '3001-5000数量')
				ws.write(0, 7, '3001-5000百分比')
				ws.write(0, 8, '5001-7000数量')
				ws.write(0, 9, '5001-7000百分比')
				ws.write(0, 10, '大于7001数量')
				ws.write(0, 11, '大于7001百分比')
				ws.write(0, 12, '总数量')				
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,13):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('推荐组合转角柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response			
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'independent_design_b'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['tp1'] = int(row[2])
				itemdict['ptp1'] = row[3]
				itemdict['tp2'] = int(row[4])
				itemdict['ptp2'] = row[5]
				itemdict['tp3'] = int(row[6])
				itemdict['ptp3'] = row[7]
				itemdict['tp4'] = int(row[8])
				itemdict['ptp4'] = row[9]
				itemdict['tp5'] = int(row[10])
				itemdict['ptp5'] = row[11]
				itemdict['tpall'] = int(row[12])
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data)
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)
	else:
		res['rows'] = list()
		res['total'] = 0
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)

# 获取统计datagrid中显示的数据，及导出相应数据到EXCEL。本方法设计拙劣臃肿，重复代码较多，不过因六种组合都有差异，这样写会较为清晰易维护。
def design_statistics_data(request):
	def GetPercent(num, total): 
		if total == 0:
			return '0.00%'
		else:
			ret = float(num)/float(total)*100
			#logging.warning('ret:%s' %ret)
			return '%s%%' %round(ret,2)
	def get_percent_list(lt):
		sum = len(lt)
		new_list = list()
		for item in lt:
			count = 0
			for item2 in lt:
				if item2 == item:
					count = count + 1
			new_list.append(GetPercent(count, sum))
		return new_list
	res = dict()
	if request.REQUEST.get('marketid') and request.REQUEST.get('time1') and request.REQUEST.get('time2') and request.REQUEST.get('price1') and request.REQUEST.get('price2') and request.REQUEST.get('designtype') \
	and request.REQUEST.get('x') and request.REQUEST.get('y1') and request.REQUEST.get('z') \
	and request.REQUEST.get('lx') and request.REQUEST.get('rx') and request.REQUEST.get('y2') and request.REQUEST.get('lz') and request.REQUEST.get('rz') and 'design_name_like' in request.REQUEST:		
		marketid = request.REQUEST.get('marketid')
		time1 = request.REQUEST.get('time1')
		time1 = int(time.mktime(time.strptime(time1,'%Y-%m-%d')))
		time2 = request.REQUEST.get('time2')
		# 将最大天数加上一天，以方便后面获取包含当天的数据
		time2 = int(time.mktime(time.strptime(time2,'%Y-%m-%d'))) + 60*60*24
		#logging.warning('time1:%s\ttime2:%s' %(time1,time2))
		price1 = float(request.REQUEST.get('price1'))
		# 单位转化成（角）
		price1 = price1 * 10				
		price2 = float(request.REQUEST.get('price2'))
		# 单位转化成（角）
		price2 = price2 * 10
		logging.warning('price1:%s\tprice2:%s' %(price1,price2))
		designtype = request.REQUEST.get('designtype')
		logging.warning('designtype:%s' %designtype)
		x = int(request.REQUEST.get('x'))
		y1 = int(request.REQUEST.get('y1'))		
		z = int(request.REQUEST.get('z'))
		lx = int(request.REQUEST.get('lx'))
		rx = int(request.REQUEST.get('rx'))
		y2 = int(request.REQUEST.get('y2'))
		lz = int(request.REQUEST.get('lz'))
		rz = int(request.REQUEST.get('rz'))
		design_name_like = request.REQUEST.get('design_name_like')
		page = 1
		rows = 100000000
		if 'page' in request.REQUEST and 'rows' in request.REQUEST:
			page = int(request.REQUEST.get('page'))
			rows = int(request.REQUEST.get('rows'))		
		min = (page-1) * rows
		max = page * rows
		logging.warning("min:%s\tmax:%s" %(min,max))
		if designtype== "default_design_a":
			sql = "SELECT markets.marketid,marketname,designid,designname1,x,y,z,price,updatetime FROM default_design_a,markets WHERE markets.marketid=default_design_a.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND designname1 LIKE '%%%s%%' " \
				%(marketid, price1, price2, design_name_like)
 			if x != -1:
 				sql += " and x=%s " %x
 			if y1 != -1:
 				sql += " and y=%s " %y1
 			if z != -1:
 				sql += " and z=%s " %z
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			x_list = list()
			y1_list = list()
			z_list = list()
			data_list = list()
			if data:
				click_sum = 0
				buy_sum = 0
				click_list = list()
				buy_list = list()				
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(row[3])
					cur.execute("SELECT COUNT(*) FROM default_design_statistics WHERE designtype='a' AND ACTION='click' AND designid=%s AND createtime BETWEEN %s AND %s " %(row[2], time1, time2))
					click_count = int(cur.fetchall()[0][0])
					row_list.append(click_count);					
					click_list.append(click_count)
					click_sum += click_count
					row_list.append(0);
					cur.execute("SELECT COUNT(*) FROM default_design_statistics WHERE designtype='a' AND ACTION='buy' AND designid=%s AND createtime BETWEEN %s AND %s " %(row[2], time1, time2))
					buy_count = int(cur.fetchall()[0][0])
					row_list.append(buy_count)
					buy_list.append(buy_count)
					buy_sum += buy_count	
					row_list.append(0)	
					row_list.append(row[4])
					x_list.append(int(row[4]))
					row_list.append(0)
					row_list.append(row[5])
					y1_list.append(int(row[5]))
					row_list.append(0)
					row_list.append(row[6])
					z_list.append(int(row[6]))
					row_list.append(0)
					row_list.append(row[7])
					row_list.append(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(row[8]))))
					data_list.append(row_list)
				x_list_new = get_percent_list(x_list)
				y1_list_new = get_percent_list(y1_list)
				z_list_new = get_percent_list(z_list)
				for i in range(0,len(data_list)):
					data_list[i][5] = GetPercent(int(click_list[i]), int(click_sum))
					data_list[i][7] = GetPercent(int(buy_list[i]),int(buy_sum))
					data_list[i][9] = x_list_new[i]
					data_list[i][11] = y1_list_new[i]
					data_list[i][13] = z_list_new[i]	
			logging.warning("data_list.size:%s" %len(data_list))
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('组合数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '组合号')
				ws.write(0, 3, '组合名')
				ws.write(0, 4, '点击数')
				ws.write(0, 5, '点击数百分比')
				ws.write(0, 6, '购买数')
				ws.write(0, 7, '购买数百分比')
				ws.write(0, 8, '宽度')
				ws.write(0, 9, '宽度百分比')
				ws.write(0, 10, '高度')
				ws.write(0, 11, '高度百分比')
				ws.write(0, 12, '深度')
				ws.write(0, 13, '深度百分比')
				ws.write(0, 14, '总价格')
				ws.write(0, 15, '日期')			
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,16):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('推荐组合一字柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response			
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'default_design_a'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['designid'] = row[2]
				itemdict['designname'] = row[3]
				itemdict['clickcount'] = row[4]
				itemdict['clickcount_percent'] = row[5]
				itemdict['buycount'] = row[6]
				itemdict['buycount_percent'] = row[7]
				itemdict['x'] = row[8]
				itemdict['x_percent'] = row[9]
				itemdict['y1'] = row[10]
				itemdict['y1_percent'] = row[11]
				itemdict['z'] = row[12]
				itemdict['z_percent'] = row[13]
				itemdict['price'] = '%.1f' %(row[14]/10.0)
				itemdict['datetime'] = row[15]
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data)
		elif designtype== "default_design_b":
			sql = "SELECT markets.marketid,marketname,designid,designname1,lx,rx,y,lz,rz,price,updatetime FROM markets,default_design_b WHERE markets.marketid=default_design_b.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND designname1 LIKE '%%%s%%' " \
				%(marketid, price1, price2, design_name_like)
 			if lx != -1:
 				sql += " and lx=%s " %lx
 			if rx != -1:
 				sql += " and rx=%s " %rx 				
 			if y2 != -1:
 				sql += " and y=%s " %y2
 			if lz != -1:
 				sql += " and lz=%s " %lz
 			if rz != -1:
 				sql += " and rz=%s " %rz	
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			rx_list = list()
			lx_list = list()
			y2_list = list()
			rz_list = list()
			lz_list = list()
			data_list = list()
			if data:
				click_sum = 0
				buy_sum = 0
				click_list = list()
				buy_list = list()				
				for row in data:
					row_list = list()
					row_list.append(row[0])
					logging.warning("row[0]:%s" %row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(row[3])
					cur.execute("SELECT COUNT(*) FROM default_design_statistics WHERE designtype='b' AND ACTION='click' AND designid=%s AND createtime BETWEEN %s AND %s " %(row[2], time1, time2))
					click_count = int(cur.fetchall()[0][0])
					row_list.append(click_count);					
					click_list.append(click_count)
					click_sum += click_count
					row_list.append(0);
					cur.execute("SELECT COUNT(*) FROM default_design_statistics WHERE designtype='b' AND ACTION='buy' AND designid=%s AND createtime BETWEEN %s AND %s " %(row[2], time1, time2))
					buy_count = int(cur.fetchall()[0][0])
					row_list.append(buy_count)
					buy_list.append(buy_count)
					buy_sum += buy_count	
					row_list.append(0)	
					row_list.append(row[4])
					rx_list.append(int(row[4]))
					row_list.append(0)	
					row_list.append(row[5])
					lx_list.append(int(row[5]))					
					row_list.append(0)
					row_list.append(row[6])
					y2_list.append(int(row[6]))
					row_list.append(0)
					row_list.append(row[7])
					rz_list.append(int(row[7]))
					row_list.append(0)
					row_list.append(row[8])
					lz_list.append(int(row[8]))
					row_list.append(0)					
					row_list.append(row[9])
					row_list.append(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(row[10]))))
					data_list.append(row_list)
				rx_list_new = get_percent_list(rx_list)
				logging.warning(lx_list)
				lx_list_new = get_percent_list(lx_list)
				y2_list_new = get_percent_list(y2_list)
				rz_list_new = get_percent_list(rz_list)
				lz_list_new = get_percent_list(lz_list)
				for i in range(0,len(data_list)):
					data_list[i][5] = GetPercent(int(click_list[i]), int(click_sum))
					data_list[i][7] = GetPercent(int(buy_list[i]),int(buy_sum))
					data_list[i][9] = rx_list_new[i]
					data_list[i][11] = lx_list_new[i]
					data_list[i][13] = y2_list_new[i]
					data_list[i][15] = rz_list_new[i]	
					data_list[i][17] = lz_list_new[i]	
			logging.warning("data_list.size:%s" %len(data_list))			
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('组合数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '组合号')
				ws.write(0, 3, '组合名')
				ws.write(0, 4, '点击数')
				ws.write(0, 5, '点击数百分比')
				ws.write(0, 6, '购买数')
				ws.write(0, 7, '购买数百分比')
				ws.write(0, 8, '左侧柜宽度')
				ws.write(0, 9, '左侧柜宽度百分比')
				ws.write(0, 10, '右侧柜宽度')
				ws.write(0, 11, '右侧柜宽度百分比')
				ws.write(0, 12, '高度')
				ws.write(0, 13, '高度百分比')
				ws.write(0, 14, '左侧柜深度')
				ws.write(0, 15, '左侧柜深度百分比')
				ws.write(0, 16, '右侧柜深度')
				ws.write(0, 17, '右侧柜深度百分比')
				ws.write(0, 18, '总价格')		
				ws.write(0, 19, '日期')			
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,20):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('推荐组合转角柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response				
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'default_design_b'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['designid'] = row[2]
				itemdict['designname'] = row[3]
				itemdict['clickcount'] = row[4]
				itemdict['clickcount_percent'] = row[5]
				itemdict['buycount'] = row[6]
				itemdict['buycount_percent'] = row[7]
				itemdict['rx'] = row[8]
				itemdict['rx_percent'] = row[9]
				itemdict['lx'] = row[10]
				itemdict['lx_percent'] = row[11]				
				itemdict['y2'] = row[12]
				itemdict['y2_percent'] = row[13]
				itemdict['rz'] = row[14]
				itemdict['rz_percent'] = row[15]
				itemdict['lz'] = row[16]
				itemdict['lz_percent'] = row[17]				
				itemdict['price'] = '%.1f' %(row[18]/10.0)
				itemdict['datetime'] = row[19]
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data_list)
		elif designtype== "reference_design_a":		
			sql = "SELECT markets.marketid,marketname,designid,base_designid,x,y,z,price,createtime FROM markets,reference_design_a WHERE markets.marketid=reference_design_a.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s " \
				%(marketid, price1, price2, time1, time2)
 			if x != -1:
 				sql += " and x=%s " %x
 			if y1 != -1:
 				sql += " and y=%s " %y1
 			if z != -1:
 				sql += " and z=%s " %z
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			x_list = list()
			y1_list = list()
			z_list = list()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(row[3])
					row_list.append(row[4])
					x_list.append(int(row[4]))
					row_list.append(0)
					row_list.append(row[5])
					y1_list.append(int(row[5]))
					row_list.append(0)
					row_list.append(row[6])
					z_list.append(int(row[6]))
					row_list.append(0)
					sql = "SELECT count(*) FROM markets,reference_design_a WHERE markets.marketid=reference_design_a.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s and x=%s and y=%s and z=%s" 
					cur.execute( sql %(marketid, price1, price2, time1, time2,row[4],row[5],row[6]))
					xyzsize = int(cur.fetchall()[0][0])
					row_list.append(xyzsize)
					row_list.append(0)
					row_list.append(row[7])
					row_list.append(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(row[8]))))
					data_list.append(row_list)
				x_list_new = get_percent_list(x_list)
				y1_list_new = get_percent_list(y1_list)
				z_list_new = get_percent_list(z_list)
				cur.execute( "select count(*) from markets,reference_design_a WHERE markets.marketid=reference_design_a.marketid and markets.marketid='%s' AND createtime BETWEEN %s AND %s" %(marketid, time1, time2))
				xyzsum = int(cur.fetchall()[0][0])
				for i in range(0,len(data_list)):
					data_list[i][5] = x_list_new[i]
					data_list[i][7] = y1_list_new[i]
					data_list[i][9] = z_list_new[i]	
					data_list[i][11] = GetPercent(int(data_list[i][10]),int(xyzsum))	
			logging.warning("data_list.size:%s" %len(data_list))			
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('组合数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '组合号')
				ws.write(0, 3, '所属组合')
				ws.write(0, 4, '宽度')
				ws.write(0, 5, '宽度百分比')
				ws.write(0, 6, '高度')
				ws.write(0, 7, '高度百分比')
				ws.write(0, 8, '深度')
				ws.write(0, 9, '深度百分比')
				ws.write(0, 10, '尺寸组合数')
				ws.write(0, 11, '尺寸组合百分比')
				ws.write(0, 12, '总价格')		
				ws.write(0, 13, '日期')			
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,14):
						ws.write(excel_row, index, row[index])				
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('组合设计一字柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response				
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'reference_design_a'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['designid'] = row[2]
				itemdict['basedesignname'] = row[3]
				itemdict['x'] = row[4]
				itemdict['x_percent'] = row[5]
				itemdict['y1'] = row[6]
				itemdict['y1_percent'] = row[7]
				itemdict['z'] = row[8]
				itemdict['z_percent'] = row[9]
				itemdict['size'] = row[10]
				itemdict['size_percent'] = row[11]
				itemdict['price'] = '%.1f' %(row[12]/10.0)
				itemdict['datetime'] = row[13]
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data_list)			
		elif designtype== "reference_design_b":
			sql = "SELECT markets.marketid,marketname,designid,base_designid,lx,rx,y,lz,rz,price,createtime FROM markets,reference_design_b WHERE markets.marketid=reference_design_b.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s " \
				%(marketid, price1, price2, time1, time2)
 			if lx != -1:
 				sql += " and lx=%s " %lx
 			if rx != -1:
 				sql += " and rx=%s " %rx 				
 			if y2 != -1:
 				sql += " and y=%s " %y2
 			if lz != -1:
 				sql += " and lz=%s " %lz
 			if rz != -1:
 				sql += " and rz=%s " %rz	
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()		
			rx_list = list()
			lx_list = list()
			y2_list = list()
			rz_list = list()
			lz_list = list()
			data_list = list()
			if data:		
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(row[3])
					row_list.append(row[4])
					rx_list.append(int(row[4]))
					row_list.append(0)	
					row_list.append(row[5])
					lx_list.append(int(row[5]))					
					row_list.append(0)
					row_list.append(row[6])
					y2_list.append(int(row[6]))
					row_list.append(0)
					row_list.append(row[7])
					rz_list.append(int(row[7]))
					row_list.append(0)
					row_list.append(row[8])
					lz_list.append(int(row[8]))
					row_list.append(0)		
					sql = "SELECT count(*) FROM markets,reference_design_b WHERE markets.marketid=reference_design_b.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s and lx=%s and rx=%s and y=%s and lz=%s and rz=%s" 
					cur.execute( sql %(marketid, price1, price2, time1, time2,row[4],row[5],row[6],row[7],row[8]))
					xyzsize = int(cur.fetchall()[0][0])	
					row_list.append(xyzsize)
					row_list.append(0)		
					row_list.append(row[9])
					row_list.append(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(row[10]))))
					data_list.append(row_list)
				rx_list_new = get_percent_list(rx_list)
				logging.warning(lx_list)
				lx_list_new = get_percent_list(lx_list)
				y2_list_new = get_percent_list(y2_list)
				rz_list_new = get_percent_list(rz_list)
				lz_list_new = get_percent_list(lz_list)
				cur.execute( "select count(*) from markets,reference_design_b WHERE markets.marketid=reference_design_b.marketid and markets.marketid='%s' AND createtime BETWEEN %s AND %s" %(marketid, time1, time2))
				xyzsum = int(cur.fetchall()[0][0])
				for i in range(0,len(data_list)):
					data_list[i][5] = rx_list_new[i]
					data_list[i][7] = lx_list_new[i]
					data_list[i][9] = y2_list_new[i]
					data_list[i][11] = rz_list_new[i]	
					data_list[i][13] = lz_list_new[i]	
					data_list[i][15] = GetPercent(int(data_list[i][14]),int(xyzsum))
			logging.warning("data_list.size:%s" %len(data_list))					
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('组合数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '组合号')
				ws.write(0, 3, '所属组合')
				ws.write(0, 4, '左侧柜宽度')
				ws.write(0, 5, '左侧柜宽度百分比')
				ws.write(0, 6, '右侧柜宽度')
				ws.write(0, 7, '右侧柜宽度百分比')
				ws.write(0, 8, '高度')
				ws.write(0, 9, '高度百分比')
				ws.write(0, 10, '左侧柜深度')
				ws.write(0, 11, '左侧柜深度百分比')
				ws.write(0, 12, '右侧柜深度')
				ws.write(0, 13, '右侧柜深度百分比')
				ws.write(0, 14, '尺寸组合数')
				ws.write(0, 15, '尺寸组合百分比')
				ws.write(0, 16, '总价格')		
				ws.write(0, 17, '日期')			
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,18):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('组合设计转角柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response				
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'reference_design_b'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['designid'] = row[2]
				itemdict['basedesignname'] = row[3]
				itemdict['rx'] = row[4]
				itemdict['rx_percent'] = row[5]
				itemdict['lx'] = row[6]
				itemdict['lx_percent'] = row[7]				
				itemdict['y2'] = row[8]
				itemdict['y2_percent'] = row[9]
				itemdict['rz'] = row[10]
				itemdict['rz_percent'] = row[11]
				itemdict['lz'] = row[12]
				itemdict['lz_percent'] = row[13]	
				itemdict['size'] = row[14]
				itemdict['size_percent'] = row[15]			
				itemdict['price'] = '%.1f' %(row[16]/10.0)
				itemdict['datetime'] = row[17]
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data_list)
		elif designtype== "independent_design_a":
			sql = "SELECT markets.marketid,marketname,designid,x,y,z,price,createtime FROM markets,independent_design_a WHERE markets.marketid=independent_design_a.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s " \
				%(marketid, price1, price2, time1, time2)
 			if x != -1:
 				sql += " and x=%s " %x
 			if y1 != -1:
 				sql += " and y=%s " %y1
 			if z != -1:
 				sql += " and z=%s " %z
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()
			x_list = list()
			y1_list = list()
			z_list = list()
			data_list = list()
			if data:			
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(row[3])
					x_list.append(int(row[3]))
					row_list.append(0)
					row_list.append(row[4])
					y1_list.append(int(row[4]))
					row_list.append(0)
					row_list.append(row[5])
					z_list.append(int(row[5]))
					row_list.append(0)
					sql = "SELECT count(*) FROM markets,independent_design_a WHERE markets.marketid=independent_design_a.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s and x=%s and y=%s and z=%s" 
					cur.execute( sql %(marketid, price1, price2, time1, time2,row[3],row[4],row[5]))
					xyzsize = int(cur.fetchall()[0][0])
					row_list.append(xyzsize)
					row_list.append(0)
					row_list.append(row[6])
					row_list.append(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(row[7]))))
					data_list.append(row_list)
				x_list_new = get_percent_list(x_list)
				y1_list_new = get_percent_list(y1_list)
				z_list_new = get_percent_list(z_list)
				cur.execute( "select count(*) from markets,independent_design_a WHERE markets.marketid=independent_design_a.marketid and markets.marketid='%s' AND createtime BETWEEN %s AND %s" %(marketid, time1, time2))
				xyzsum = int(cur.fetchall()[0][0])
				for i in range(0,len(data_list)):
					data_list[i][4] = x_list_new[i]
					data_list[i][6] = y1_list_new[i]
					data_list[i][8] = z_list_new[i]	
					data_list[i][10] = GetPercent(int(data_list[i][9]),int(xyzsum))
			logging.warning("data_list.size:%s" %len(data_list))					
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('组合数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '组合号')
				ws.write(0, 3, '宽度')
				ws.write(0, 4, '宽度百分比')
				ws.write(0, 5, '高度')
				ws.write(0, 6, '高度百分比')
				ws.write(0, 7, '深度')
				ws.write(0, 8, '深度百分比')
				ws.write(0, 9, '尺寸组合数')
				ws.write(0, 10, '尺寸组合百分比')
				ws.write(0, 11, '总价格')		
				ws.write(0, 12, '日期')			
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,13):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('自主设计一字柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
				wb.save(response)
				return response
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'independent_design_a'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['designid'] = row[2]
				itemdict['x'] = row[3]
				itemdict['x_percent'] = row[4]
				itemdict['y1'] = row[5]
				itemdict['y1_percent'] = row[6]
				itemdict['z'] = row[7]
				itemdict['z_percent'] = row[8]
				itemdict['size'] = row[9]
				itemdict['size_percent'] = row[10]
				itemdict['price'] = '%.1f' %(row[11]/10.0)
				itemdict['datetime'] = row[12]
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data_list)			
		elif designtype== "independent_design_b":
			sql = "SELECT markets.marketid,marketname,designid,lx,rx,y,lz,rz,price,createtime FROM markets,independent_design_b WHERE markets.marketid=independent_design_b.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s " \
				%(marketid, price1, price2, time1, time2)
 			if lx != -1:
 				sql += " and lx=%s " %lx
 			if rx != -1:
 				sql += " and rx=%s " %rx 				
 			if y2 != -1:
 				sql += " and y=%s " %y2
 			if lz != -1:
 				sql += " and lz=%s " %lz
 			if rz != -1:
 				sql += " and rz=%s " %rz	
			logging.warning("sql:%s" %sql)
			cur = connection.cursor()
			cur.execute(sql)
			data = cur.fetchall()		
			rx_list = list()
			lx_list = list()
			y2_list = list()
			rz_list = list()
			lz_list = list()
			data_list = list()
			if data:		
				for row in data:
					row_list = list()
					row_list.append(row[0])
					row_list.append(row[1])
					row_list.append(row[2])
					row_list.append(row[3])
					rx_list.append(int(row[3]))
					row_list.append(0)	
					row_list.append(row[4])
					lx_list.append(int(row[5]))					
					row_list.append(0)
					row_list.append(row[5])
					y2_list.append(int(row[5]))
					row_list.append(0)
					row_list.append(row[6])
					rz_list.append(int(row[6]))
					row_list.append(0)
					row_list.append(row[7])
					lz_list.append(int(row[7]))
					row_list.append(0)		
					sql = "SELECT count(*) FROM markets,independent_design_b WHERE markets.marketid=independent_design_b.marketid and markets.marketid='%s' and price BETWEEN %s AND %s AND createtime BETWEEN %s AND %s and lx=%s and rx=%s and y=%s and lz=%s and rz=%s" 
					cur.execute( sql %(marketid, price1, price2, time1, time2,row[3],row[4],row[5],row[6],row[7]))
					xyzsize = int(cur.fetchall()[0][0])	
					row_list.append(xyzsize)
					row_list.append(0)					
					row_list.append(row[8])
					row_list.append(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(row[9]))))
					data_list.append(row_list)
				rx_list_new = get_percent_list(rx_list)
				logging.warning(lx_list)
				lx_list_new = get_percent_list(lx_list)
				y2_list_new = get_percent_list(y2_list)
				rz_list_new = get_percent_list(rz_list)
				lz_list_new = get_percent_list(lz_list)
				cur.execute( "select count(*) from markets,independent_design_b WHERE markets.marketid=independent_design_b.marketid and markets.marketid='%s' AND createtime BETWEEN %s AND %s" %(marketid, time1, time2))
				xyzsum = int(cur.fetchall()[0][0])
				for i in range(0,len(data_list)):
					data_list[i][4] = rx_list_new[i]
					data_list[i][6] = lx_list_new[i]
					data_list[i][8] = y2_list_new[i]
					data_list[i][10] = rz_list_new[i]	
					data_list[i][12] = lz_list_new[i]
					data_list[i][14] = GetPercent(int(data_list[i][13]),int(xyzsum))	
			logging.warning("data_list.size:%s" %len(data_list))					
			if 'excel' in request.REQUEST:
				# 创建 Workbook 时，如果需要写入中文，请使用 utf-8 编码，默认是 unicode 编码。
				wb = xlwt.Workbook(encoding='utf-8')
				ws = wb.add_sheet('组合数据统计')
				ws.write(0, 0, '商场编号')
				ws.write(0, 1, '商场')
				ws.write(0, 2, '组合号')
				ws.write(0, 3, '左侧柜宽度')
				ws.write(0, 4, '左侧柜宽度百分比')
				ws.write(0, 5, '右侧柜宽度')
				ws.write(0, 6, '右侧柜宽度百分比')
				ws.write(0, 7, '高度')
				ws.write(0, 8, '高度百分比')
				ws.write(0, 9, '左侧柜深度')
				ws.write(0, 10, '左侧柜深度百分比')
				ws.write(0, 11, '右侧柜深度')
				ws.write(0, 12, '右侧柜深度百分比')
				ws.write(0, 13, '尺寸组合数')
				ws.write(0, 14, '尺寸组合百分比')
				ws.write(0, 15, '总价格')		
				ws.write(0, 16, '日期')			
				excel_row = 0
				for row in data_list:
					excel_row += 1
					for index in range(0,17):
						ws.write(excel_row, index, row[index])
				response = HttpResponse(content_type='application/vnd.ms-excel')
				response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.xls' %(marketid, urlencode('自主设计转角柜'), time.strftime('%Y%m%d',time.localtime(time.time())))
 				wb.save(response)
				return response
			rowslist = list()
			for row in data_list[min:max]:
				itemdict = dict()
				itemdict['designtype'] = 'independent_design_b'
				itemdict['marketid'] = row[0]
				itemdict['marketname'] = row[1]
				itemdict['designid'] = row[2]
				itemdict['rx'] = row[3]
				itemdict['rx_percent'] = row[4]
				itemdict['lx'] = row[5]
				itemdict['lx_percent'] = row[6]				
				itemdict['y2'] = row[7]
				itemdict['y2_percent'] = row[8]
				itemdict['rz'] = row[9]
				itemdict['rz_percent'] = row[10]
				itemdict['lz'] = row[11]
				itemdict['lz_percent'] = row[12]	
				itemdict['size'] = row[13]
				itemdict['size_percent'] = row[14]			
				itemdict['price'] = '%.1f' %(row[15]/10.0)
				itemdict['datetime'] = row[16]
				rowslist.append(itemdict)
			res['rows'] = rowslist
			res['total'] = len(data_list)
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)
	else:
		res['rows'] = list()
		res['total'] = 0
		res = json.dumps(res).decode("unicode-escape")
		logging.warning("res ->  %s " %res)
		return HttpResponse(res)
