function WidthPercent(percent)
{
	return document.documentElement.clientWidth * percent; 
}

function HeightPercent(percent)
{
	return document.documentElement.clientHeight * percent;
}	

$.extend($.fn.validatebox.defaults.rules, {    
	midLength: {    
		validator: function(value,param){    
			if(value.length >= param[0] && value.length <= param[1])
				return true;    				
			},    
		message: '长度介于 {0} ~ {1} 之间'   
	}    
}); 


function trim(str){   
	return str.replace(/^(\s|\u00A0)+/,'').replace(/(\s|\u00A0)+$/,'');   
}