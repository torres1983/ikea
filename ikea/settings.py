# coding:utf-8
"""
Django settings for ikea project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
#from django.db import connection

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
# 项目绝对根路径
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))


# ������־����ɫ

# 配置文志显示的格式
import logging
logging.basicConfig(
        #format = '\033[33m%(asctime)s \033[32m%(filename)s [line:%(lineno)d] \r\n\033[31m%(message)s\033[0m \r\n',
        format = '%(asctime)s %(filename)s [line:%(lineno)d] \nlog: %(message)s \r\n',
        datefmt = '%Y-%m-%d %H:%M:%S',
        )



# 配置模板路径
TEMPLATE_DIRS = (
        #os.path.join(os.path.dirname(__file__),'../templates').replace('\\','/'),
        os.path.join(BASE_DIR,'templates').replace('\\','/'),
        )

# 设置静态文件路径
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'static').replace('\\','/'),
    os.path.join(BASE_DIR, 'media').replace('\\','/'),
)
#STATIC_PATH = os.path.join(BASE_DIR, 'static').replace('\\','/')
STATIC_URL = '/static/'

# 设置上传文件路径
MEDIA_ROOT = os.path.join(BASE_DIR, 'media').replace('\\','/')
MEDIA_URL = '/media/'


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'jt&z_rjiv2yl7iopg2*@i&18df1byuu9*_x4l^!!9!pb_@t2=#'
#secret_key='jFE93j;2[290-eiw.KEiwN2s3[d;/.q[eIW^y#e=+Iei*@Mn<qW5o'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True


TEMPLATE_CONTEXT_PROCESSORS = (
      #'django.core.context_processors.auth',    
      "django.core.context_processors.request",                
)

ALLOWED_HOSTS = []

APPEND_SLASH = True         # URL结尾以 / 结束
# Application definition

INSTALLED_APPS = (
    #'django.contrib.admin',
    #'django.contrib.auth',
    #'django.contrib.contenttypes',
    'django.contrib.sessions',
    #'django.contrib.messages',
    'django.contrib.staticfiles',
    'manager',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

#server_domain = "http://127.0.0.1"         # 主机域名或IP

ROOT_URLCONF = 'ikea.urls'

WSGI_APPLICATION = 'ikea.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

#"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ikea',
        'USER': 'root',
        'PASSWORD': 'ikea2014',
        #'HOST': 'newbilitygames.oicp.net',
        'HOST': '121.41.22.152',
        'PORT': '3306',
    }
}



"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
"""

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

#TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


