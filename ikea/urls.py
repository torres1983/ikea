#coding:utf-8

import django
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import TemplateView
from manager.views import *

admin.autodiscover()

import settings
#from manager.views import undefined,index2



# 下面是后台使用的接口
urlpatterns = patterns('',
    #url(r'^static/(?P<path>.*)/$', 'django.views.static.serve', {'document_root': settings.STATIC_URL},name="static"),
    url(r'^/media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT},name="media"),
    url(r'^login/$', TemplateView.as_view(template_name='login.html')),
    url(r'^markets/$', TemplateView.as_view(template_name='markets.html')),
    url(r'^units/$', requires_login(units)),
    url(r'^market_list_with_all/$', requires_login(market_list_with_all)),
    url(r'^activity/$', requires_login(get_activity)),
    url(r'^update_activity/$', requires_login(update_activity)),
    #url(r'^default_design_a/$',  TemplateView.as_view(template_name='default_design_a.html')),
    url(r'^default_design_a/$', requires_login(default_design_a)),
    url(r'^default_design_a_data/$', requires_login(default_design_a_data)),
    url(r'^edit_default_design_a/$',  requires_login(edit_default_design_a)),
    url(r'^save_default_design_a/$',  requires_login(save_default_design_a)),
    url(r'^delete_default_design_a/$',  requires_login(delete_default_design_a)),
    url(r'^default_design_b/$', requires_login(default_design_b)),
    url(r'^default_design_b_data/$', requires_login(default_design_b_data)),
    url(r'^edit_default_design_b/$',  requires_login(edit_default_design_b)),
    url(r'^save_default_design_b/$',  requires_login(save_default_design_b)),
    url(r'^delete_default_design_b/$',  requires_login(delete_default_design_b)),
    url(r'^$', requires_login(index)),    
    url(r'^index/$', requires_login(index)),
    url(r'^logout/$', requires_login(logout)),
    url(r'^login_handle/$', login_handle),
    url(r'^update_pwd_handle/$', requires_login(update_pwd_handle)),    
    url(r'^markets_data/$', requires_login(markets_data)), 
    url(r'^users_data/$', requires_login(users_data)), 
    url(r'^market_save/$', requires_login(market_save)), 
    url(r'^market_remove/$', requires_login(market_remove)), 
    url(r'^market_list/$', requires_login(market_list)), 
    url(r'^permission_list/$', requires_login(permission_list)),    
    url(r'^user_remove/$', requires_login(user_remove)), 
    url(r'^user_save/$', requires_login(user_save)),     
    
    url(r'^questions/$', TemplateView.as_view(template_name='questions.html')),
    url(r'^questiontype/$', TemplateView.as_view(template_name='questiontype.html')),
    url(r'^questiontype_data/$', requires_login(questiontype_data)),
    url(r'^questiontype_save/$', requires_login(questiontype_save)),
    url(r'^questiontype_remove/$', requires_login(questiontype_remove)),
    url(r'^questiontype_list/$', requires_login(questiontype_list)),
    url(r'^questions_data/$', requires_login(questions_data)),
    url(r'^question_add/$', requires_login(question_add)),
    url(r'^question_remove/$', requires_login(question_remove)),
    url(r'^upload_questions/$', requires_login(upload_questions)),
    url(r'^questionnaire_generate/$', requires_login(questionnaire_generate)),
    url(r'^questionnaire/$', requires_login(questionnaire)),
    #url(r'^questionnaire/$', TemplateView.as_view(template_name='questionnaire.html')),
    url(r'^questionnaires_by_marketid/$', requires_login(questionnaires_by_marketid)),
    url(r'^time_by_questionnaireid/$', requires_login(time_by_questionnaireid)),    
    url(r'^questionnaire_by_questionnaireid_or_surveyid/$', requires_login(questionnaire_by_questionnaireid_or_surveyid)),
    url(r'^question_selected_info/$', requires_login(question_selected_info)),
    url(r'^set_questionnaire/$', requires_login(set_questionnaire)),
    url(r'^save_questionnaire_to_excel/$', requires_login(save_questionnaire_to_excel)),
    
    url(r'^units_data/$', requires_login(units_data)),
    url(r'^unit_save/$', requires_login(unit_save)),
    url(r'^statistics/$', requires_login(statistics)),
    url(r'^statistics_price/$', requires_login(statistics_price)),
    url(r'^design_statistics_data/$', requires_login(design_statistics_data)),
    url(r'^design_statistics_price_data/$', requires_login(design_statistics_price_data)),
)

# 下面是协议处理接口
urlpatterns += patterns('manager.protocol_views',
    url(r'^protocol/upload_test/$','upload_test'),
    url(r'^protocol/login_test/$','login_test'),   
    url(r'^protocol/get_time/$','get_time'),
    url(r'^protocol/get_market_list/$','get_market_list'),
    url(r'^protocol/login/$','login'),
    url(r'^protocol/get_questionnaire/$','get_questionnaire'),
    url(r'^protocol/submit_questionnaire/$','submit_questionnaire'),
    url(r'^protocol/get_activity/$','get_activity'),
    url(r'^protocol/get_unit_price/$','get_unit_price'),
    url(r'^protocol/send_email/$','send_email'),
    url(r'^protocol/get_default_design_a/$','get_default_design_a'),
    url(r'^protocol/get_default_design_b/$','get_default_design_b'),
    url(r'^protocol/add_default_design_a/$','add_default_design_a'),
    url(r'^protocol/add_default_design_b/$','add_default_design_b'),
    url(r'^protocol/remove_default_design_a/$','remove_default_design_a'),
    url(r'^protocol/remove_default_design_b/$','remove_default_design_b'),    
    url(r'^protocol/default_design_statistics/$','default_design_statistics'),
    url(r'^protocol/add_independent_design_a/$','add_independent_design_a'),
    url(r'^protocol/add_independent_design_b/$','add_independent_design_b'),
    url(r'^protocol/add_reference_design_a/$','add_reference_design_a'),
    url(r'^protocol/add_reference_design_b/$','add_reference_design_b'),
)

# 下面是用HTML表单来实现的模拟客户端的测试协议页面
urlpatterns += patterns('',
    url(r'^test/$',TemplateView.as_view(template_name='test/test.html')),
    url(r'^test/get_time/$',TemplateView.as_view(template_name='test/get_time.html')),
    url(r'^test/get_market_list/$',TemplateView.as_view(template_name='test/get_market_list.html')),
    url(r'^test/login/$',TemplateView.as_view(template_name='test/login.html')),
    url(r'^test/get_questionnaire/$',TemplateView.as_view(template_name='test/get_questionnaire.html')),
    url(r'^test/submit_questionnaire/$',TemplateView.as_view(template_name='test/submit_questionnaire.html')),
    url(r'^test/get_activity/$',TemplateView.as_view(template_name='test/get_activity.html')),
    url(r'^test/get_unit_price/$',TemplateView.as_view(template_name='test/get_unit_price.html')),
    url(r'^test/send_email/$',TemplateView.as_view(template_name='test/send_email.html')),
    url(r'^test/get_default_design_a/$',TemplateView.as_view(template_name='test/get_default_design_a.html')),
    url(r'^test/get_default_design_b/$',TemplateView.as_view(template_name='test/get_default_design_b.html')),
    url(r'^test/add_default_design_a/$',TemplateView.as_view(template_name='test/add_default_design_a.html')),
    url(r'^test/add_default_design_b/$',TemplateView.as_view(template_name='test/add_default_design_b.html')),
    url(r'^test/default_design_statistics/$',TemplateView.as_view(template_name='test/default_design_statistics.html')),
    url(r'^test/add_independent_design_a/$',TemplateView.as_view(template_name='test/add_independent_design_a.html')),
    url(r'^test/add_independent_design_b/$',TemplateView.as_view(template_name='test/add_independent_design_b.html')),
    url(r'^test/add_reference_design_a/$',TemplateView.as_view(template_name='test/add_reference_design_a.html')),
    url(r'^test/add_reference_design_b/$',TemplateView.as_view(template_name='test/add_reference_design_b.html')),
    
    # 下面是用来载入一些初始化数据的模板
    url(r'^tool/load_units_data/$',TemplateView.as_view(template_name='test/load_units_data.html')),
    url(r'^tool/load_default_design_data/$',TemplateView.as_view(template_name='test/load_default_design_data.html')),
)

 # 下面是用来载入一些初始化数据的方法
urlpatterns += patterns('manager.tool_views',
    url(r'^tool/load_units_data_handle/$','load_units_data_handle'),
    url(r'^tool/load_default_design_data_handle/$','load_default_design_data_handle'),
)
